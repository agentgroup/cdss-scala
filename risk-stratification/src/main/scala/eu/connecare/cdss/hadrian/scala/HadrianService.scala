package eu.connecare.cdss.hadrian.scala

import java.io.FileInputStream
import java.io.FileOutputStream

import scala.collection.JavaConverters.asScalaIteratorConverter

import org.apache.logging.log4j.scala.Logging

import com.opendatagroup.hadrian.jvmcompiler.PFAEmitEngine
import com.opendatagroup.hadrian.jvmcompiler.PFAEngine
import com.opendatagroup.hadrian.data.`package`.CsvOutputDataStream

class HadrianService(private var engine: PFAEngine[AnyRef, AnyRef]) extends Logging with Equals {

  //  private var engine: PFAEngine[AnyRef, AnyRef] = _
  private var inputData: Iterator[AnyRef] = _
  private var outputData: CsvOutputDataStream = _

  //  /**
  //   * Load the PFA file into a scoring engine
  //   */
  //  def loadEngine(pfaEngineName: String): PFAEngine[AnyRef, AnyRef] = {
  //    engine = PFAEngine.fromJson(new FileInputStream(pfaEngineName)).head
  //    return engine
  //  }

  def getEngine(): PFAEngine[AnyRef, AnyRef] = {
    return engine
  }

  //  def this(pfaEngineName: String) {
  //    this(PFAEngine.fromJson(new FileInputStream(pfaEngineName)).head)
  //  }

  def this(pfaEngineFile: FileInputStream) {
    this(PFAEngine.fromJson(pfaEngineFile).head)
  }

  /**
   * Feed an input file in csv format
   */
  def feedWithCsv(inputDataFile: FileInputStream): Iterator[AnyRef] = {
    inputData = engine.csvInputIterator(inputDataFile).asScala
    return inputData
  }

  /**
   * Set output data stream
   */
  def setOutputStream(outputDataName: String): CsvOutputDataStream = {
    outputData = engine.csvOutputDataStream(new FileOutputStream(outputDataName))
    return outputData
  }

  /**
   * Run engine in batch mode on CSV input
   */
  def predictBatchCsv(shouldLog: Boolean) = {
    // do the begin phase
    engine.begin()
    // run the engine over the input data
    engine match {
      case emitEngine: PFAEmitEngine[_, _] =>
        if (shouldLog)
          emitEngine.emit = { x: AnyRef => emitToFileAndLog(outputData, x) }
        else
          emitEngine.emit = { x: AnyRef => outputData.append(x) }
        while (inputData.hasNext)
          engine.action(inputData.next())
      case _ =>
        while (inputData.hasNext) {
          val toLog = engine.action(inputData.next())
          outputData.append(toLog)
          if (shouldLog)
            logger.info(toLog)
        }
    }
    // do the end phase
    engine.end()
    // remember to close the output stream so that it flushes!
    outputData.close()
  }

  private def emitToFileAndLog(outputData: CsvOutputDataStream, x: AnyRef) = {
    outputData.append(x)
    logger.info(x)
  }

  //  def main(args: Array[String]) = {
  //    runEngine(args)
  //  }

  def canEqual(other: Any) = {
    other.isInstanceOf[eu.connecare.cdss.hadrian.scala.HadrianService]
  }

  override def equals(other: Any) = {
    other match {
      case that: eu.connecare.cdss.hadrian.scala.HadrianService => HadrianService.super.equals(that) && that.canEqual(HadrianService.this) && engine == that.engine
      case _ => false
    }
  }

  override def hashCode() = {
    val prime = 41
    prime * HadrianService.super.hashCode() + engine.hashCode
  }

}