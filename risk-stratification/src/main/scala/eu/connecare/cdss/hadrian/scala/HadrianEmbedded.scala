package eu.connecare.cdss.hadrian.scala

import java.io.FileInputStream
import java.io.FileOutputStream

import com.opendatagroup.hadrian.jvmcompiler.PFAEmitEngine
import com.opendatagroup.hadrian.jvmcompiler.PFAEngine

object HadrianEmbedded {
  def main(args: Array[String]) {
    // read command-line arguments
    val Array(pfaEngineName, inputDataName, outputDataName) = args

    // load the PFA file into a scoring engine
    val engine = PFAEngine.fromJson(new FileInputStream(pfaEngineName)).head

    // make an iterator for the input data
    val inputData = engine.csvInputIterator(new FileInputStream(inputDataName))

    // make an appender for the output data
    val outputData = engine.csvOutputDataStream(new FileOutputStream(outputDataName))

    // do the begin phase
    engine.begin()

    // run the engine over the input data
    engine match {
      case emitEngine: PFAEmitEngine[_, _] =>
        emitEngine.emit = { x: AnyRef => outputData.append(x) }
        while (inputData.hasNext)
          engine.action(inputData.next())

      case _ =>
        while (inputData.hasNext)
          outputData.append(engine.action(inputData.next()))
    }

    // do the end phase
    engine.end()

    // remember to close the output stream so that it flushes!
    outputData.close()
  }
}