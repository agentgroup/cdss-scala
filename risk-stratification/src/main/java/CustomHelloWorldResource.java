import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 * Created by {user} on 21 nov 2016 17:15:41
 */

/**
 * @author ste
 *
 */
@Path("/test/custom/{user.name}/{salute}")
public final class CustomHelloWorldResource {

    @GET
    @Produces("text/plain")
    public String getSalute(@PathParam("user.name") final String userName, @PathParam("salute") final String sal) {
	return new StringBuilder(sal).append(" ").append(userName).append(" :)").toString();
    }

}
