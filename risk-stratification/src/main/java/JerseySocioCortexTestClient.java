import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;

/**
 *
 */

/**
 * @author ste
 *
 */
public final class JerseySocioCortexTestClient {

    /**
     * @param args
     */
    public static void main(final String[] args) {

	//	SslConfigurator sslConfig = SslConfigurator.newInstance().trustStoreFile("./truststore_client")
	//	        .trustStorePassword("$up3r$3cr3t").keyStoreFile("./keystore_client").keyPassword("$3cr3t$up3r");
	//	SSLContext sslContext = sslConfig.createSSLContext();
	//	Client client = ClientBuilder.newBuilder().sslContext(sslContext).build();

	final HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic("sc@connecare.eu", "1u9xp9adgumuh");

	final Client client = ClientBuilder.newClient();
	client.register(feature);
	final WebTarget webTarget = client.target("https://server.sociocortex.com/api/v1/").path("workspaces");
	final Invocation.Builder invocationBuilder = webTarget.request(MediaType.TEXT_PLAIN_TYPE);
	//	invocationBuilder.header("authorization", "Basic c2NAY29ubmVjYXJlLmV1OjF1OXhwOWFkZ3VtdWg=");
	final Response response = invocationBuilder.get();
	System.out.println(response.getStatus());
	System.out.println(response.readEntity(String.class));
    }

}
