package eu.connecare.cdss.hadrian.rest;

import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;

import io.swagger.config.ScannerFactory;
import io.swagger.jaxrs.config.BeanConfig;
import io.swagger.jaxrs.config.ReflectiveJaxrsScanner;

/**
 * 
 */

/**
 * @author sm
 *
 */
public final class HadrianJerseyConfig extends ResourceConfig {

	public HadrianJerseyConfig() {
		/*
		 * From https://github.com/swagger-api/swagger-core/issues/927
		 */
		ReflectiveJaxrsScanner scanner = new ReflectiveJaxrsScanner();
		scanner.setResourcePackage("eu.connecare.cdss.hadrian.rest");
		ScannerFactory.setScanner(scanner);
		/* */
		BeanConfig beanConfig = new BeanConfig();
		beanConfig.setSchemes(new String[] { "https" });
		beanConfig.setHost("localhost:9998");
		beanConfig.setBasePath("/hadrian");
		// beanConfig.setBasePath("https://" + this.adaptedIp + ":" +
		// this.endpointTcp + "/hadrian");
		// beanConfig.setResourcePackage("eu.connecare.cdss.hadrian.rest");
		beanConfig.setScan(true);
		beanConfig.setPrettyPrint(true);
		registerClasses(eu.connecare.cdss.hadrian.rest.HadrianServiceResource.class,
				/*
				 * for Swagger 2.0 from
				 * https://github.com/swagger-api/swagger-core/wiki/Swagger-Core
				 * -Jersey-2.X-Project-Setup-1.5#using-a-custom-application-
				 * subclass
				 */
				io.swagger.jaxrs.listing.ApiListingResource.class, io.swagger.jaxrs.listing.SwaggerSerializers.class);
		packages("org.glassfish.jersey.examples.jackson", "io.swagger.jaxrs.listing", "eu.connecare.cdss.hadrian.rest");
		register(JacksonFeature.class);
		register(MultiPartFeature.class);
	}

}
