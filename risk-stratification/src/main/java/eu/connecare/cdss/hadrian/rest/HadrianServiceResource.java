/**
 * 
 */
package eu.connecare.cdss.hadrian.rest;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

import javax.inject.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import eu.connecare.cdss.hadrian.BlackBoxModel;
import eu.connecare.cdss.hadrian.HadrianServiceProxy;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Contact;
import io.swagger.annotations.Info;
import io.swagger.annotations.License;
import io.swagger.annotations.ResponseHeader;
import io.swagger.annotations.SwaggerDefinition;

/**
 * @author sm
 *
 */
@Singleton
@javax.ws.rs.Path("hadrian")
@Api(value = "hadrian")
@Produces({ "application/json",
		"application/xml" }) /* overridden by method-level annotations */
@SwaggerDefinition(info = @Info(description = "CDSS service -> PFA models", version = "0.0.1", title = "The CDSS-PFA API",
		// termsOfService = "http://theweatherapi.io/terms.html",
		contact = @Contact(name = "Stefano Mariani", email = "stefano.mariani@unimore.it", url = "https://cdss-risk-stratification.herokuapp.com/"), license = @License(name = "Apache 2.0", url = "http://www.apache.org/licenses/LICENSE-2.0")), consumes = {
				"application/json", "application/xml" }, produces = { "application/json",
						"application/xml" }, schemes = { SwaggerDefinition.Scheme.HTTPS })
public final class HadrianServiceResource {

	private static final Logger LOGGER = LogManager.getLogger(HadrianServiceResource.class);
	private final HadrianServiceProxy hadrian;

	public HadrianServiceResource() {
		this.hadrian = new HadrianServiceProxy();
	}

	@POST
	@javax.ws.rs.Path("model/pfa")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.TEXT_PLAIN)
	/* code = 200 is default */
	@ApiOperation(value = "Adds a PFA-encoded model to the CDSS", notes = "The name of the uploaded file is taken as the logical name of the model (actual ID is a UUID). Besides this, a description is welcome.", response = String.class, responseHeaders = @ResponseHeader(name = "Location", description = "The UUID to be appended to the URL path for referring to the desired PFA model resource ", response = String.class))
	@ApiResponses(value = { @ApiResponse(code = 500, message = "IOException") })
	/*
	 * required = true is default for PathParams, see
	 * http://docs.swagger.io/swagger-core/current/apidocs/index.html?io/swagger
	 * /annotations/ApiParam.html
	 */
	public Response uploadModelPfa(
			@ApiParam(value = "The file storing the model in PFA format", required = true) @FormDataParam("file") final InputStream file,
			@FormDataParam("file") final FormDataContentDisposition fileDisposition,
			@ApiParam(value = "A description of the file storing the model in PFA format", required = false) @DefaultValue("") @FormDataParam("description") final String description,
			@Context UriInfo uriInfo) {
		Response resp = null;
		try {
			final UUID modelID = this.hadrian.uploadModelPfa(fileDisposition.getFileName(), file, description);
			final URI modelURI = uriInfo.getAbsolutePathBuilder().path(modelID.toString()).build();
			resp = Response.created(modelURI)
					.entity("Model " + fileDisposition.getFileName() + " uploaded successfully").build();
		} catch (IOException e) {
			resp = Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getLocalizedMessage()).type("text/plain")
					.build();
		}
		return resp;
	}

	@GET
	@javax.ws.rs.Path("models")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public List<BlackBoxModel> listModels() {
		return this.hadrian.listModels();
	}

	@GET
	@javax.ws.rs.Path("model/{modelID}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public BlackBoxModel modelDescriptor(@PathParam("modelID") final UUID modelID) {
		return this.hadrian.modelDescriptor(modelID); // re-throw?
	}

	@GET
	@javax.ws.rs.Path("model/pfa/{modelID}")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response downloadModelPfa(@PathParam("modelID") final UUID modelID) {
		File file;
		Response resp = null;
		try {
			file = this.hadrian.downloadModelPfa(modelID).toFile();
			resp = Response.ok(file).header("Content-Disposition", "attachment; filename=" + file.getName()).build();
		} catch (IOException e) {
			resp = Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getLocalizedMessage()).type("text/plain")
					.build();
		}
		return resp;
	}

	@GET
	@javax.ws.rs.Path("model/pfa/redis/{modelID}")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response downloadModelPfaRedis(@PathParam("modelID") final UUID modelID) {
		Response resp = null;
		try {
			if (Files.exists(Paths.get("redis/temp/"))) {
				FileUtils.cleanDirectory(Paths.get("redis/temp/").toFile());
			}
		} catch (IOException e) {
			LOGGER.debug("IOException: " + e.getLocalizedMessage());
			resp = Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getLocalizedMessage()).type("text/plain")
					.build();
		}
		byte[] fileContent;
		try {
			fileContent = this.hadrian.downloadModelPfaRedis(modelID);
			final Path redisTemp = Files.createDirectories(Paths.get("redis/temp/"));
			final File file = Paths.get(redisTemp.toString(), "pfa$" + modelID).toFile();
			/*
			 * bad hack, I know, God will forgive me, will you?
			 */
			FileUtils.writeByteArrayToFile(file, fileContent);
			LOGGER.debug("Created temporary file: " + file);
			/* */
			resp = Response.ok(file).header("Content-Disposition", "attachment; filename=" + file.getName()).build();
			// Files.delete(file.toPath()); /* if deleting here caller does not
			// get the attachment */
		} catch (IOException e) {
			resp = Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getLocalizedMessage()).type("text/plain")
					.build();
		}
		return resp;
	}

	@DELETE
	@javax.ws.rs.Path("model/{modelID}")
	// @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces(MediaType.TEXT_PLAIN)
	public Response deleteModel(@PathParam("modelID") final UUID modelID) {
		Response resp = null;
		try {
			if (this.hadrian.deleteModel(modelID)) {
				resp = Response.ok("Model " + modelID + " deleted successfully").build();
			} else {
				resp = Response.status(Status.NO_CONTENT).build();
			}
		} catch (IOException e) {
			resp = Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getLocalizedMessage()).type("text/plain")
					.build();
		}
		return resp;
	}

	@POST
	@javax.ws.rs.Path("model/{modelID}/csv")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response predictBatchCsv(@PathParam("modelID") final UUID modelID,
			@FormDataParam("file") final InputStream dataFile,
			@FormDataParam("file") final FormDataContentDisposition fileDisposition,
			@DefaultValue("false") @QueryParam("shouldLog") final Boolean shouldLog) {
		File results;
		Response resp = null;
		try {
			results = this.hadrian.predictBatchCsv(modelID, fileDisposition.getFileName(), dataFile, shouldLog)
					.toFile();
			resp = Response.ok(results).header("Content-Disposition", "attachment; filename=" + results.getName())
					.build();
		} catch (IOException e) {
			LOGGER.info(e.toString());
			resp = Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getLocalizedMessage()).type("text/plain")
					.build();
		}
		return resp;
	}

}
