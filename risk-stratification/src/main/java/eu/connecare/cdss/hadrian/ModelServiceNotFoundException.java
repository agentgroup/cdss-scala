/**
 * 
 */
package eu.connecare.cdss.hadrian;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

/**
 * @author sm
 *
 */
public final class ModelServiceNotFoundException extends WebApplicationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final String msg;

	public ModelServiceNotFoundException() {
		super(Response.status(Status.NOT_FOUND).build());
		this.msg = "";
	}

	public ModelServiceNotFoundException(final String message) {
		super(Response.status(Status.NOT_FOUND).entity(message).type("text/plain").build());
		this.msg = message;
	}

	/**
	 * @return the msg
	 */
	@Override
	public String getMessage() {
		return this.msg;
	}

}
