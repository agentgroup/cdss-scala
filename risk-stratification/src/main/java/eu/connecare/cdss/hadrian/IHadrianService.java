package eu.connecare.cdss.hadrian;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.List;
import java.util.UUID;

public interface IHadrianService {

	UUID uploadModelPfa(String name, InputStream fileContent, String description) throws IOException;

	UUID uploadModelRds(String name, InputStream fileContent, String description) throws IOException;

	List<BlackBoxModel> listModels();

	BlackBoxModel modelDescriptor(UUID modelID) throws ModelDescriptorNotFoundException;

	Path downloadModelPfa(UUID modelID) throws PfaModelNotFoundException, IOException;

	Path downloadModelRds(UUID modelID) throws RdsModelNotFoundException, IOException;

	boolean deleteModel(UUID modelID) throws IOException;

	Prediction<?> predictSingle(UUID modelID, DataPoint<?> data);

	Path predictBatchCsv(UUID modelID, String fileName, InputStream dataStream, Boolean shouldLog)
			throws PredictionServiceNotFoundException, IOException;

}