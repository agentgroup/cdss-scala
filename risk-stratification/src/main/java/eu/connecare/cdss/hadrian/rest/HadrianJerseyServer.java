package eu.connecare.cdss.hadrian.rest;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.ws.rs.core.UriBuilder;

import org.apache.commons.validator.routines.InetAddressValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;

import io.swagger.jaxrs.config.BeanConfig;

/**
 * Created by {user} on 21 nov 2016 15:51:55
 */

/**
 * @author ste
 *
 */
public final class HadrianJerseyServer {

	private static final Logger LOGGER = LogManager.getLogger(HadrianJerseyServer.class);
	private static final String DEFAULT_PROPERTY_FILE = "risk-stratification/src/main/resources/hadrian-server.properties";
	// private static final String DEFAULT_PROPERTY_FILE =
	// "src/main/resources/hadrian-server.properties";
	private static final String ENDPOINT_IP = "endpoint.ip";
	private static final String ENDPOINT_TCP = "endpoint.tcp";
	private static final String RESOURCE_CLAZZ = "resource.clazz";
	private String endpointIp;
	private String adaptedIp;
	private int endpointTcp;
	private Class<?> resourceClazz;
	private HttpServer server;
	public static final int MIN_PORT = 1024;
	public static final int MAX_PORT = 65535;

	/**
	 * <p>
	 * Validates arguments as passed by a standard Java {@code main()} method.
	 * </p>
	 * <p>
	 * The method expects a single argument, which is interpreted as the
	 * filepath where the configuration file {@code *.properties} necessary for
	 * setting up {@code this} server can be found.
	 * </p>
	 * <p>
	 * If no arguments are given, the method falls back to the default
	 * configuration file {@code server-env.properties}.
	 * </p>
	 * <p>
	 * If more than one argument is given, any other argument is ignored.
	 * </p>
	 * 
	 * @param args
	 *            A single argument is expected: the filepath to a
	 *            {@code *.properties} configuration file
	 * @return The {@link String} representing the filepath actually considered
	 */
	public static String validateArgs(final String[] args) {
		switch (args.length) {
		case 0:
			HadrianJerseyServer.LOGGER.warn(String.format("No properties file given, falling back to default <%s>",
					HadrianJerseyServer.DEFAULT_PROPERTY_FILE));
			return HadrianJerseyServer.DEFAULT_PROPERTY_FILE;
		case 1:
			HadrianJerseyServer.LOGGER.info(String.format("Accepting user given property file <%s>", args[0]));
			return args[0];
		default:
			HadrianJerseyServer.LOGGER.warn(String.format(
					"Too many arguments (usage: $> java JerseySimpleServer path/to/file.properties), considering only <%s> (first one)",
					args[0]));
			return args[0];
		}
	}

	/**
	 * Loads the {@code *.properties} file.
	 * 
	 * @param propsFile
	 *            The filepath where the {@code *.properties} can be found
	 * @return The {@link Properties} loaded
	 * @throws IOException
	 *             If the {@code *.properties} file cannot be found, read, or
	 *             any other I/O exception occurs
	 */
	public static Properties loadProps(final String propsFile) throws IOException {
		final Properties props = new Properties();
		props.load(Files.newInputStream(Paths.get(propsFile)));
		return props;
	}

	/**
	 * Parses the loaded {@link Properties} to initialise {@code this} server.
	 * 
	 * @param props
	 *            The {@link Properties} to parse
	 * @throws InvalidPropertiesFormatException
	 *             If the {@link Properties} are badly formatted, their keys
	 *             unknown, or their values invalid (e.g. non valid IP address
	 *             or reserved TCP port number)
	 */
	public void parseProps(final Properties props) throws InvalidPropertiesFormatException {
		this.endpointIp = props.getProperty(HadrianJerseyServer.ENDPOINT_IP);
		HadrianJerseyServer.LOGGER.debug(String.format("Parsed endpoint.ip: %s", this.endpointIp));
		final InetAddressValidator inetVal = InetAddressValidator.getInstance();
		if (this.endpointIp == null || this.endpointIp.isEmpty() || !inetVal.isValidInet4Address(this.endpointIp)) {
			HadrianJerseyServer.LOGGER.debug(String.format("Parsed endpoint.ip: %s is invalid!", this.endpointIp));
			throw new InvalidPropertiesFormatException(String.format("Given <%s> in props file <%s> is NOT valid",
					HadrianJerseyServer.ENDPOINT_IP, props));
		}
		if ("127.0.0.1".equals(this.endpointIp)) {
			this.adaptedIp = "localhost/";
		} else {
			this.adaptedIp = this.endpointIp;
		}
		HadrianJerseyServer.LOGGER.debug(String.format("Parsed adaptedIp: %s", this.adaptedIp));
		String endTcp = props.getProperty(HadrianJerseyServer.ENDPOINT_TCP);
		// FIXME Ugly, exception thrown is the same, can't re-wire conditional
		// logic to have single throw?
		if (endTcp != null && !endTcp.isEmpty()) {
			/*
			 * Tentative fix from
			 * https://stackoverflow.com/questions/34066878/error-r10-boot-
			 * timeout-web-process-failed-to-bind-to-port-within-60-second Maybe
			 * necessary to do the same with ip 1) heroku config:set
			 * SERVER_URL="https://yourappname.herokuapp.com" 2) String
			 * SERVER_URL = System.getenv("SERVER_URL"); UPDATE: from
			 * https://stackoverflow.com/questions/36751071/heroku-web-process-
			 * failed-to-bind-to-port-within-90-seconds-of-launch-tootall
			 * correct binding for step 2) is String host = "0.0.0.0"; and no
			 * need for step 1)
			 */
			if ("heroku_port".equals(endTcp)) {
				endTcp = System.getenv("PORT");
				HadrianJerseyServer.LOGGER.debug(String.format("HEROKU DEPLoYMENT: binding to tcp port: %s", endTcp));
			}
			final int portno = Integer.parseInt(endTcp);
			if (portno > MIN_PORT && portno <= MAX_PORT) {
				this.endpointTcp = Integer.parseInt(endTcp);
			} else {
				throw new InvalidPropertiesFormatException(String.format("Given <%s> in props file <%s> is NOT valid",
						HadrianJerseyServer.ENDPOINT_TCP, props));
			}
		} else {
			throw new InvalidPropertiesFormatException(String.format("Given <%s> in props file <%s> is NOT valid",
					HadrianJerseyServer.ENDPOINT_TCP, props));
		}
		final String resClazz = props.getProperty(HadrianJerseyServer.RESOURCE_CLAZZ);
		if (resClazz == null || resClazz.isEmpty()) {
			throw new InvalidPropertiesFormatException(String.format("Given <%s> in props file <%s> is NOT valid",
					HadrianJerseyServer.RESOURCE_CLAZZ, props));
		}
		try {
			this.resourceClazz = Class.forName(resClazz);
		} catch (final ClassNotFoundException e) {
			final InvalidPropertiesFormatException ex = new InvalidPropertiesFormatException(String.format(
					"Given <%s> in props file <%s> is NOT on classpath", HadrianJerseyServer.RESOURCE_CLAZZ, props));
			ex.initCause(e);
			throw ex;
		}
	}

	/**
	 * Deploys {@code this} server according to the configuration file given.
	 * 
	 * @return {@code this} server
	 * @throws IOException
	 *             If the underlying Grizzly container cannot start the server
	 *             for any reason
	 *
	 */
	public HadrianJerseyServer deploy() throws IOException {
		HadrianJerseyServer.LOGGER
				.debug(String.format("Deploying to ip: %s tcp: %s", this.adaptedIp, this.endpointTcp));
		/*
		 * Seems we need protocol info
		 * http://www.oracle.com/webfolder/technetwork/tutorials/obe/java/
		 * griz_jersey_intro/Grizzly-Jersey-Intro.html#section6
		 */
		final URI baseUri = UriBuilder.fromUri("https://" + this.adaptedIp).port(this.endpointTcp).build();
		HadrianJerseyServer.LOGGER.debug(String.format("Deploying to %s : %s", baseUri.getHost(), baseUri.getPort()));
		/*
		 * From https://github.com/swagger-api/swagger-core/issues/927
		 */
		// ReflectiveJaxrsScanner scanner = new ReflectiveJaxrsScanner();
		// scanner.setResourcePackage("eu.connecare.cdss.hadrian.rest");
		// ScannerFactory.setScanner(scanner);
		/* */
		BeanConfig beanConfig = new BeanConfig();
		// beanConfig.setVersion("0.0.1");
		// beanConfig.setSchemes(new String[]{"https"});
		beanConfig.setHost(this.adaptedIp + ":" + this.endpointTcp);
		beanConfig.setBasePath("/api-docs");
		// beanConfig.setResourcePackage("eu.connecare.cdss.hadrian.rest");
		beanConfig.setScan(true);
		final ResourceConfig config = new ResourceConfig(this.resourceClazz,
				io.swagger.jaxrs.listing.ApiListingResource.class, io.swagger.jaxrs.listing.SwaggerSerializers.class)
						.packages("org.glassfish.jersey.examples.jackson").register(JacksonFeature.class)
						.register(MultiPartFeature.class);
		/*
		 * As per
		 * https://stackoverflow.com/questions/33681371/swagger-documentation-
		 * with-jax-rs-jersey-2-and-grizzly
		 */
		// ServletContainer sc = new ServletContainer(config);
		// this.server = GrizzlyWebContainerFactory.create(baseUri, sc, null,
		// null);
		/* */
		this.server = GrizzlyHttpServerFactory.createHttpServer(baseUri, config);
		this.server.start();
		return this;
	}

	/**
	 * Gracefully shuts down {@code this} server.
	 * 
	 * @param gracePeriod
	 *            The period to gracefully wait for shutdown
	 * @param timeUnit
	 *            The {@link TimeUnit} according to which the
	 *            {@code gracePeriod} is expressed
	 * @return {@code this} server
	 */
	public HadrianJerseyServer shutdown(final long gracePeriod, final TimeUnit timeUnit) {
		if (this.server != null && this.server.isStarted()) {
			this.server.shutdown(gracePeriod, timeUnit);
			HadrianJerseyServer.LOGGER.info(
					String.format("Jersey RESTful endpoint on <%s:%s> exposing resource <%s> successfully stopped",
							this.adaptedIp, this.endpointTcp, this.getResourceClazz()));
		} else {
			HadrianJerseyServer.LOGGER.warn(String.format("Jersey RESTful endpoint on <%s:%s> is already NOT active",
					this.adaptedIp, this.endpointTcp));
		}
		return this;
	}

	/**
	 * Checks whether {@code this} server is currently up&running or not.
	 * 
	 * @return {@code true} if {@code this} server is NOT up&running,
	 *         {@code false} otherwise
	 */
	public boolean isDown() {
		return !this.server.isStarted();
	}

	/**
	 * Exposes the IP address where {@code this} RESTful endpoint is available.
	 * 
	 * @return The IP address where {@code this} RESTful endpoint is available
	 */
	public String getEndpointIp() {
		return this.endpointIp;
	}

	/**
	 * Exposes the TCP port number where {@code this} RESTful endpoint is
	 * available.
	 * 
	 * @return The TCP port number where {@code this} RESTful endpoint is
	 *         available
	 */
	public int getEndpointTcp() {
		return this.endpointTcp;
	}

	/**
	 * <p>
	 * Exposes the "adapted IP" where {@code this} RESTful endpoint is
	 * available.
	 * </p>
	 * <p>
	 * The adapted IP is simply:
	 * <ul>
	 * <li>"http://localhost/" instead of "127.0.0.1", in case the configuration
	 * file {@code *.properties} specifies "127.0.0.1"
	 * <li>the same IP address specified in the configuration file
	 * {@code *.properties} otherwise
	 * </ul>
	 * </p>
	 * <p>
	 * This adaptation is a workaround for a testing issue.
	 * {@link InetAddressValidator} does not recognises "http://localhost/" as a
	 * valid IP address, but that's the address Jersey expects when working
	 * locally.
	 * </p>
	 * <p>
	 * Thus, the workaround is that in the configuration file
	 * {@code *.properties} we put "127.0.0.1", then we translate it in
	 * "http://localhost/" after validation, to succesfully configure
	 * {@code this} jersey server while still passing
	 * {@link InetAddressValidator} checks.
	 * </p>
	 * 
	 * @return The adapted IP, that is, "http://localhost/" in case the
	 *         configuration file {@code *.properties} specifies "127.0.0.1"
	 */
	public String getAdaptedIp() {
		return this.adaptedIp;
	}

	/**
	 * Exposes the {@link String} name of the REST resource (Java class)
	 * {@code this} RESTful endpoint makes available.
	 * 
	 * @return The {@link String} name of the REST resource (Java class)
	 *         {@code this} RESTful endpoint makes available
	 */
	public String getResourceClazz() {
		return this.resourceClazz.getName();
	}

}
