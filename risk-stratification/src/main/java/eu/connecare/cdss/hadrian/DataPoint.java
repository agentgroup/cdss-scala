/**
 * 
 */
package eu.connecare.cdss.hadrian;

/**
 * @author sm
 *
 */
public final class DataPoint<K> {

	private final K data;

	public DataPoint(final K data) {
		this.data = data;
	}

	public K get() {
		return this.data;
	}

}
