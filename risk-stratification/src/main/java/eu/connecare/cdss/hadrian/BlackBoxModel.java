/**
 * 
 */
package eu.connecare.cdss.hadrian;

import java.util.UUID;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author sm
 *
 */
@XmlRootElement
public final class BlackBoxModel { // TODO don't like non final fields, is there
									// another way? i.e. through Jackson
									// mappers?

	private UUID id;
	private String name;
	private String description;
	private boolean asPFA;
	private boolean asR;
	private boolean asPMML;

	public BlackBoxModel() {
	}

	public BlackBoxModel(final String name, final String description) {
		this.name = name;
		this.description = description;
		this.id = UUID.randomUUID();
		this.asPFA = false;
		this.asR = false;
		this.asPMML = false;
	}

	/**
	 * @return the id
	 */
	public UUID getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(UUID id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the asPFA
	 */
	public boolean hasPFA() {
		return this.asPFA;
	}

	/**
	 * @param asPFA
	 *            the asPFA to set
	 */
	public void asPFA(boolean asPFA) {
		this.asPFA = asPFA;
	}

	/**
	 * @return the asR
	 */
	public boolean hasR() {
		return this.asR;
	}

	/**
	 * @param asR
	 *            the asR to set
	 */
	public void asR(boolean asR) {
		this.asR = asR;
	}

	/**
	 * @return the asPMML
	 */
	public boolean hasPMML() {
		return this.asPMML;
	}

	/**
	 * @param asPMML
	 *            the asPMML to set
	 */
	public void asPMML(boolean asPMML) {
		this.asPMML = asPMML;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof BlackBoxModel)) {
			return false;
		}
		BlackBoxModel other = (BlackBoxModel) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format("BlackBoxModel [id=%s, name=%s, description=%s, asPFA=%s, asR=%s, asPMML=%s]", id, name,
				description, asPFA, asR, asPMML);
	}

}
