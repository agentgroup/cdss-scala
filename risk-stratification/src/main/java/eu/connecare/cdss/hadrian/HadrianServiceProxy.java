/**
 * 
 */
package eu.connecare.cdss.hadrian;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import org.apache.commons.validator.routines.InetAddressValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.collect.ImmutableList;

import eu.connecare.cdss.hadrian.rest.HadrianJerseyServer;
import eu.connecare.cdss.hadrian.scala.HadrianService;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.ScanResult;

/**
 * @author sm
 *
 */
public final class HadrianServiceProxy implements IHadrianService {

	private static final Logger LOGGER = LogManager.getLogger(HadrianServiceProxy.class);
//	private static final String DEFAULT_PROPERTY_FILE = "src/main/resources/redis-server.properties";
	private static final String DEFAULT_PROPERTY_FILE = "risk-stratification/src/main/resources/redis-server.properties";
	private static final String HOST = "server.host";
	private static final String PORT = "server.port";
	private static final String MAX_TOTAL = "poolConfig.maxTotal";
	private static final String MAX_IDLE = "poolConfig.maxIdle";
	private static final String MIN_IDLE = "poolConfig.minIdle";
	private static final String TEST_ON_BORROW = "poolConfig.testOnBorrow";
	private static final String TEST_ON_RETURN = "poolConfig.testOnReturn";
	private static final String TEST_WHILE_IDLE = "poolConfig.testWhileIdle";
	private static final String MIN_EVICTABLE_IDLE = "poolConfig.minEvictableIdleTimeMillis";
	private static final String TIME_BETWEEN_EVICTION = "poolConfig.timeBetweenEvictionRunsMillis";
	private static final String NUM_TESTS_PER_EVICTION = "poolConfig.numTestsPerEvictionRun";
	private static final String BLOCK_WHEN_EXHAUSTED = "poolConfig.blockWhenExhausted";
	private Map<UUID, BlackBoxModel> modelDescriptors;
	/*
	 * A single model may have multiple representations (PFA, RDS atm)
	 */
	private Map<UUID, List<Path>> modelFiles;
	private Map<UUID, HadrianService> modelServices;
	private JedisPoolConfig poolConfig;
	private JedisPool jedisPool;

	public HadrianServiceProxy() {
		this.modelDescriptors = new HashMap<>();
		this.modelFiles = new HashMap<>();
		this.modelServices = new HashMap<>();
		this.initJedis();
	}

	/*
	 * TODO Check if model already exists (compare file content) TODO Validate
	 * input file (how for PFA? how for R?) TODO delete file on local disk
	 */
	@Override
	public UUID uploadModelPfa(final String name, final InputStream fileContent, final String description)
			throws IOException {
		final BlackBoxModel model = new BlackBoxModel(name, description);
		model.asPFA(true);
		this.modelDescriptors.put(model.getId(), model);
		LOGGER.debug("Added model descriptor " + model.getId() + " for model " + name);
		final Path dir = Files.createDirectories(Paths.get("models/pfa/"));
		final Path file = Paths.get(dir.toString(), model.getId().toString() + "$" + name);
		Files.copy(fileContent,
				file); /* create file first due to HadrianService API */
		LOGGER.debug("Created temporary file " + file + " on local disk");
		final HashMap<byte[], byte[]> filesMap = new HashMap<>();
		filesMap.put("pfa".getBytes(), Files.readAllBytes(file));
		try (final Jedis jedis = jedisPool.getResource()) {
			jedis.hmset(HadrianServiceProxy.uuidAsBytes(model.getId()), filesMap);
		}
		LOGGER.debug("Put model file " + file.getFileName() + " to Redis map " + model.getId() + " with key 'pfa'");
		List<Path> paths = this.modelFiles.get(model.getId());
		if (paths == null) {
			paths = new ArrayList<>(); // exploit side effect
			paths.add(file);
			this.modelFiles.put(model.getId(), paths);
		} else {
			paths.add(file);
		}
		LOGGER.debug("Added model file " + file + " for model " + model.getId());
		final HadrianService hadrian = new HadrianService(new FileInputStream(file.toFile()));
		this.modelServices.put(model.getId(), hadrian);
		LOGGER.info("Added Hadrian model service for model " + model.getId());
		return model.getId();
	}

	/*
	 * TODO check if PFA version already exists (how? convert then check
	 * equality of content? rely on user-supplied names?) TODO convert prior to
	 * feed HadrianService and register Hadrian model service
	 */
	@Override
	public UUID uploadModelRds(final String name, final InputStream fileContent, final String description)
			throws IOException {
		final BlackBoxModel model = new BlackBoxModel(name, description);
		model.asR(true);
		this.modelDescriptors.put(model.getId(), model);
		LOGGER.debug("Added model descriptor " + model.getId() + " for model " + name);
		final Path dir = Files.createDirectories(Paths.get("models/r/"));
		final Path file = Paths.get(dir.toString(), model.getId().toString() + "$" + name);
		Files.copy(fileContent,
				file); /* create file first due to HadrianService API */
		LOGGER.debug("Created temporary file " + file + " on local disk");
		final HashMap<byte[], byte[]> filesMap = new HashMap<>();
		filesMap.put("r".getBytes(), Files.readAllBytes(file));
		try (final Jedis jedis = jedisPool.getResource()) {
			jedis.hmset(HadrianServiceProxy.uuidAsBytes(model.getId()), filesMap);
		}
		LOGGER.debug("Put model file " + file.getFileName() + " to Redis map " + model.getId() + " with key 'r'");
		List<Path> paths = this.modelFiles.get(model.getId());
		if (paths == null) {
			paths = new ArrayList<>(); // exploit side effect
			paths.add(file);
			this.modelFiles.put(model.getId(), paths);
		} else {
			paths.add(file);
		}
		LOGGER.debug("Added model file " + file + " for model " + model.getId());
		final HadrianService hadrian = new HadrianService(new FileInputStream(file.toFile()));
		this.modelServices.put(model.getId(), hadrian);
		LOGGER.info("Added Hadrian model service for model " + model.getId());
		return model.getId();
	}

	@Override
	public List<BlackBoxModel> listModels() {
		LOGGER.info("Existing model descriptors:");
		for (BlackBoxModel bbm : this.modelDescriptors.values()) {
			LOGGER.info("\t" + bbm);
		}
		return ImmutableList.<BlackBoxModel>builder().addAll(this.modelDescriptors.values()).build();
	}

	@Override
	public BlackBoxModel modelDescriptor(final UUID modelID) throws ModelDescriptorNotFoundException {
		LOGGER.debug("Given model ID " + modelID);
		LOGGER.debug("Existing model descriptors:");
		for (BlackBoxModel bbm : this.modelDescriptors.values()) {
			LOGGER.debug("\t" + bbm);
		}
		final BlackBoxModel model = this.modelDescriptors.get(modelID);
		if (model == null) {
			throw new ModelDescriptorNotFoundException("No model exists for the given UUID " + modelID);
		} else {
			LOGGER.info("Found model descriptor " + model.toString());
			return model;
		}
	}

	/*
	 * TODO remove search for local files and return correct list of key pairs
	 */
	public List<Path> modelFiles(final UUID modelID) {
		LOGGER.debug("Given model ID " + modelID);
//		LOGGER.debug("Existing model files in Redis:");
		try (final Jedis jedis = jedisPool.getResource()) {
//			int jedisCursor = 0;
//			ScanResult<String> scanRes;
//			do {
//				scanRes = jedis.scan(String.valueOf(jedisCursor));
//				for (String dbKey : scanRes.getResult()) {
//					LOGGER.debug("\t" + dbKey);
//					for (String mapKey : jedis.hgetAll(dbKey).keySet()) {
//						LOGGER.debug("\t\t" + mapKey);
//					}
//				}
//				jedisCursor = Integer.parseInt(scanRes.getStringCursor());
//			} while (jedisCursor != 0);
			final Map<byte[], byte[]> existing = jedis.hgetAll(HadrianServiceProxy.uuidAsBytes(modelID));
			if (existing != null) {
				String fileType;
				LOGGER.info("Found model files in Redis:");
				for (byte[] bytes : jedis.hgetAll(HadrianServiceProxy.uuidAsBytes(modelID)).keySet()) {
					fileType = new String(bytes);
					LOGGER.info("\t" + modelID + " -> " + fileType);
				}
			}
		}
		LOGGER.debug("Existing model files on local disk:");
		for (List<Path> lp : this.modelFiles.values()) {
			for (Path p : lp) {
				LOGGER.debug("\t" + p);
			}
		}
		final List<Path> found = this.modelFiles.get(modelID);
		if (found != null) {
			LOGGER.info("Found model files on local disk:");
			for (Path p : found) {
				LOGGER.info("\t" + p);
			}
		}
		return found;
	}

	public HadrianService modelService(final UUID modelID) throws ModelServiceNotFoundException {
		LOGGER.debug("Given model ID " + modelID);
		LOGGER.debug("Existing model services:");
		for (HadrianService hs : this.modelServices.values()) {
			LOGGER.debug("\t" + hs);
		}
		final HadrianService service = this.modelServices.get(modelID);
		if (service == null) {
			throw new ModelServiceNotFoundException("No service exists for the given UUID");
		} else {
			LOGGER.info("Found model service: " + service.toString());
			return service;
		}
	}

	@Override
	public Path downloadModelPfa(final UUID modelID) throws PfaModelNotFoundException, IOException {
		LOGGER.debug("Given model ID " + modelID);
		LOGGER.debug("Existing model files:");
		for (Path p : this.modelFiles.get(modelID)) {
			LOGGER.debug("\t" + p);
			if (p.toString().endsWith(".pfa")) {
				LOGGER.info("Downloading model file " + p + " from local disk");
				return p;
			}
		}
		throw new PfaModelNotFoundException();
	}

	public byte[] downloadModelPfaRedis(final UUID modelID) throws PfaModelNotFoundException {
		LOGGER.debug("Given model ID " + modelID);
		String fileType;
		LOGGER.debug("Existing model files in Redis:");
		try (final Jedis jedis = jedisPool.getResource()) {
			final Map<byte[], byte[]> existing = jedis.hgetAll(HadrianServiceProxy.uuidAsBytes(modelID));
			if (existing != null) {
				for (byte[] bytes : existing.keySet()) {
					fileType = new String(bytes);
					LOGGER.debug("\t" + modelID + " -> " + fileType);
					if ("pfa".equals(fileType)) {
						byte[] pfaFile = jedis.hget(HadrianServiceProxy.uuidAsBytes(modelID), "pfa".getBytes());
						LOGGER.info("Downloading model file from Redis map " + modelID + " as pfa");
						return pfaFile;
					}
				}
			}
		}
		throw new PfaModelNotFoundException();
	}

	@Override
	public Path downloadModelRds(final UUID modelID) throws RdsModelNotFoundException, IOException {
		LOGGER.debug("Given model ID " + modelID);
		LOGGER.debug("Existing model files:");
		for (Path p : this.modelFiles.get(modelID)) {
			LOGGER.debug("\t" + p);
			if (p.toString().endsWith(".rds")) {
				LOGGER.info("Downloading model file " + p + " from local disk");
				return p;
			}
		}
		throw new RdsModelNotFoundException();
	}

	public byte[] downloadModelRdsRedis(final UUID modelID) throws RdsModelNotFoundException {
		LOGGER.debug("Given model ID " + modelID);
		String fileType;
		LOGGER.debug("Existing model files in Redis:");
		try (final Jedis jedis = jedisPool.getResource()) {
			final Map<byte[], byte[]> existing = jedis.hgetAll(HadrianServiceProxy.uuidAsBytes(modelID));
			if (existing != null) {
				for (byte[] bytes : existing.keySet()) {
					fileType = new String(bytes);
					LOGGER.debug("\t" + modelID + " -> " + fileType);
					if ("r".equals(fileType)) {
						byte[] rdsFile = jedis.hget(HadrianServiceProxy.uuidAsBytes(modelID), "r".getBytes());
						LOGGER.info("Downloading model file from Redis map " + modelID + " as rds");
						return rdsFile;
					}
				}
			}
		}
		throw new RdsModelNotFoundException();
	}

	@Override
	public boolean deleteModel(final UUID modelID) throws IOException {
		LOGGER.debug("Given model ID " + modelID);
		LOGGER.debug("Existing model descriptors:");
		for (BlackBoxModel bbm : this.modelDescriptors.values()) {
			LOGGER.debug("\t" + bbm);
		}
		final List<Path> toDelete = this.modelFiles.get(modelID);
		Boolean res = this.modelDescriptors.remove(modelID) != null && this.modelFiles.remove(modelID) != null
				&& this.modelServices.remove(modelID) != null;
		if (res) {
			try (final Jedis jedis = jedisPool.getResource()) {
				final long ok = jedis.hdel(HadrianServiceProxy.uuidAsBytes(modelID), "pfa".getBytes(), "r".getBytes());
				if (ok == 1) {
					res &= true;
				} else {
					res &= false;
				}
			}
			for (Path path : toDelete) {
				res &= Files.deleteIfExists(path);
			}
		}
		LOGGER.info("Deletion successful: " + res);
		return res;
	}

	@Override
	public Prediction<?> predictSingle(final UUID modelID, final DataPoint<?> data) {
		return null;
	}

	public List<Prediction<?>> predictBatch(final UUID modelID, final List<DataPoint<?>> data) {
		return ImmutableList.of();
	}

	public Path predictBatchJson(final UUID modelID, final Path filepath) {
		try {
			return Files.createFile(Paths.get("predictions/new/filepath"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Path predictBatchCsv(final UUID modelID, final String fileName, final InputStream dataStream,
			final Boolean shouldLog) throws PredictionServiceNotFoundException, IOException {
		LOGGER.debug("Given model ID " + modelID);
		final HadrianService hadrian = this.modelServices.get(modelID);
		if (hadrian == null) {
			throw new PredictionServiceNotFoundException();
		} else {
			final Path temp = Files.createDirectories(Paths.get("temp/"));
			final Path file = Paths.get(temp.toString(), "dataset-" + fileName);
			Files.copy(dataStream, file);
			LOGGER.debug("Created temporary file: " + file);
			hadrian.feedWithCsv(new FileInputStream(
					file.toFile())); /* return type not used */
			/*
			 * bad hack, I know, God will forgive me, will you?
			 */
			Files.delete(file);
			Path outpath = null;
			final Path dir = Files.createDirectories(Paths.get("predictions/", modelID.toString(), "/"));
			outpath = Paths.get(dir.toString(), System.currentTimeMillis() + "$" + fileName);
			LOGGER.info("Created predictions: " + outpath);
			hadrian.setOutputStream(outpath.toString());
			hadrian.predictBatchCsv(shouldLog);
			return outpath;
		}
	}

	private static byte[] uuidAsBytes(final UUID uuid) {
		ByteBuffer bb = ByteBuffer.wrap(new byte[16]);
		bb.putLong(uuid.getMostSignificantBits());
		bb.putLong(uuid.getLeastSignificantBits());
		return bb.array();
	}

	/*
	 * TODO add validation
	 */
	private static JedisPoolConfig buildPoolConfig(final Properties props) {
		final JedisPoolConfig poolConfig = new JedisPoolConfig();
		poolConfig.setMaxTotal(Integer.parseInt(props.getProperty(HadrianServiceProxy.MAX_TOTAL)));
		poolConfig.setMaxIdle(Integer.parseInt(props.getProperty(HadrianServiceProxy.MAX_IDLE)));
		poolConfig.setMinIdle(Integer.parseInt(props.getProperty(HadrianServiceProxy.MIN_IDLE)));
		poolConfig.setTestOnBorrow(Boolean.parseBoolean(props.getProperty(HadrianServiceProxy.TEST_ON_BORROW)));
		poolConfig.setTestOnReturn(Boolean.parseBoolean(props.getProperty(HadrianServiceProxy.TEST_ON_RETURN)));
		poolConfig.setTestWhileIdle(Boolean.parseBoolean(props.getProperty(HadrianServiceProxy.TEST_WHILE_IDLE)));
		poolConfig.setMinEvictableIdleTimeMillis(Duration
				.ofSeconds(Integer.parseInt(props.getProperty(HadrianServiceProxy.MIN_EVICTABLE_IDLE))).toMillis());
		poolConfig.setTimeBetweenEvictionRunsMillis(Duration
				.ofSeconds(Integer.parseInt(props.getProperty(HadrianServiceProxy.TIME_BETWEEN_EVICTION))).toMillis());
		poolConfig.setNumTestsPerEvictionRun(
				Integer.parseInt(props.getProperty(HadrianServiceProxy.NUM_TESTS_PER_EVICTION)));
		poolConfig.setBlockWhenExhausted(
				Boolean.parseBoolean(props.getProperty(HadrianServiceProxy.BLOCK_WHEN_EXHAUSTED)));
		return poolConfig;
	}

	private void initJedis() {
		Properties props = null;
		try {
			props = HadrianJerseyServer.loadProps(HadrianServiceProxy.DEFAULT_PROPERTY_FILE);
		} catch (IOException e) {
			HadrianServiceProxy.LOGGER.fatal(String.format("Cannot load given property file (<%s>) due to <%s>",
					HadrianServiceProxy.DEFAULT_PROPERTY_FILE, e));
			System.exit(-1);
		}
		final String inet = props.getProperty(HadrianServiceProxy.HOST);
		final InetAddressValidator inetVal = InetAddressValidator.getInstance();
		if (!inetVal.isValidInet4Address(inet)) {
			HadrianServiceProxy.LOGGER.fatal(String.format("Parsed server.host: %s is invalid!", inet));
			System.exit(-1);
		}
		final int portno = Integer.parseInt(props.getProperty(HadrianServiceProxy.PORT));
		if (portno <= HadrianJerseyServer.MIN_PORT || portno > HadrianJerseyServer.MAX_PORT) {
			HadrianServiceProxy.LOGGER.fatal(
					String.format("Given <%s> in props file <%s> is NOT valid", HadrianServiceProxy.PORT, props));
			System.exit(-1);
		}
		this.poolConfig = HadrianServiceProxy.buildPoolConfig(props);
		this.jedisPool = new JedisPool(poolConfig, inet, portno);
	}

}
