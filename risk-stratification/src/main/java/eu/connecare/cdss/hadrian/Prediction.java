/**
 * 
 */
package eu.connecare.cdss.hadrian;

/**
 * @author sm
 *
 */
public final class Prediction<K> {

	private final K prediction;

	public Prediction(final K prediction) {
		this.prediction = prediction;
	}

	public K get() {
		return this.prediction;
	}

}
