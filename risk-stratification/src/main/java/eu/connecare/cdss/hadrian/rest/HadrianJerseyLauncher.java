/**
 * Created by {user} on 28 nov 2016 08:15:05
 */
package eu.connecare.cdss.hadrian.rest;

import java.io.IOException;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author ste
 *
 */
public final class HadrianJerseyLauncher {

	private static final Logger LOGGER = LogManager.getLogger(HadrianJerseyLauncher.class);

	private HadrianJerseyLauncher() {
	}

	/**
	 * @param args
	 *            The filepath where the configuration file {@code *.properties}
	 *            necessary for setting up {@link HadrianJerseyServer} can be
	 *            found.
	 */
	public static void main(final String[] args) {
		final HadrianJerseyServer server = new HadrianJerseyServer();
		final String propsFile = HadrianJerseyServer.validateArgs(args);
		Properties props = null;
		try {
			props = HadrianJerseyServer.loadProps(propsFile);
		} catch (final IOException e) {
			HadrianJerseyLauncher.LOGGER
					.fatal(String.format("Cannot load given property file (<%s>) due to <%s>", propsFile, e));
			System.exit(-1);
		}
		try {
			server.parseProps(props);
		} catch (final InvalidPropertiesFormatException e) {
			HadrianJerseyLauncher.LOGGER.fatal(String.format("Cannot parse properties due to <%s>", e));
			System.exit(-1);
		}
		try {
			server.deploy();
			HadrianJerseyLauncher.LOGGER
					.info(String.format("Jersey RESTful endpoint up&running on <%s:%s> exposing resource <%s>",
							server.getAdaptedIp(), server.getEndpointTcp(), server.getResourceClazz()));
		} catch (final IOException e) {
			HadrianJerseyLauncher.LOGGER.fatal(String.format("Cannot deploy server due to <%s>", e));
		}
	}

}
