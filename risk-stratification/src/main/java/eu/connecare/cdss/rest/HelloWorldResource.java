package eu.connecare.cdss.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 * Created by {user} on 21 nov 2016 16:03:12
 */

/**
 * @author ste
 *
 */
@Path("/test/{user.name}")
public final class HelloWorldResource {

    @GET
    @Produces("text/plain")
    public static String getSalute(
            @PathParam("user.name") final String userName) {
        return new StringBuilder("Hello ").append(userName).append(" :)")
                .toString();
    }

    @POST
    @Consumes("text/plain")
    @Produces("text/plain") // ASK Does this make sense? It still works without...
    public static String postSalute(
            @PathParam("user.name") final String userName, final String test) {
        // ASK How to instantiate a CustomHelloWorldResource.class resource?
        //        URI createdUri = UriBuilder.fromResource(CustomHelloWorldResource.class)
        //                .build();
        //        return Response.created(createdUri)
        //                .entity(Entity.text(new StringBuilder(test).append(" ")
        //                        .append(sal).append(" ").append(userName).append(" :)")
        //                        .toString()))
        //                .build();
        // ASK Why the code below doesn't work?
        //        return Response.ok().entity(Entity.text(new StringBuilder(test)
        //                .append(" ").append(userName).append(" :)").toString()))
        //                .build();
        return new StringBuilder(test).append(" ").append(userName)
                .append(" :)").toString();
    }

    @POST
    @Path("/{salute}")
    @Consumes("text/plain")
    public static String postCustomSalute(
            @PathParam("user.name") final String userName,
            @PathParam("salute") final String sal, final String test) {
        return new StringBuilder(test).append(" ").append(sal).append(" ")
                .append(userName).append(" :)").toString();
    }

}
