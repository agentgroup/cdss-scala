/**
 * Created by {user} on 29 nov 2016 08:18:57
 */
package eu.connecare.cdss.rest;

/**
 * @author ste
 *
 */
public enum RESTReqs {

    //CHECKSTYLE.OFF: Variable JavaDoc
    GET, POST, PUT, DELETE;
    //CHECKSTYLE.ON: Variable JavaDoc

}
