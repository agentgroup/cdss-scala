/**
 * Created by {user} on 29 nov 2016 11:06:05
 */
package eu.connecare.cdss.rest;

/**
 * @author ste
 *
 */
public final class UnknownMediaTypeException extends Exception {

    private final String msg;

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * Constructor accepting a message {@link String} explaining {@code this}
     * exception.
     * 
     * @param message
     *            The message {@link String} explaining {@code this} exception
     */
    public UnknownMediaTypeException(final String message) {
        super();
        this.msg = message;
    }

    /**
     * @return
     * @see java.lang.Throwable#getMessage()
     */
    @Override
    public String getMessage() {
        return this.msg;
    }

}
