/**
 * Created by {user} on 29 nov 2016 08:37:29
 */
package eu.connecare.cdss.rest;

import javax.ws.rs.core.MediaType;

/**
 * @author ste
 *
 */
public enum RESTMedias {
    //CHECKSTYLE.OFF: Variable JavaDoc
    APPLICATION_ATOM_XML("application/atom+xml"), APPLICATION_FORM_URLENCODED(
            "application/x-www-form-urlencoded"), APPLICATION_JSON(
                    "application/json"), APPLICATION_OCTET_STREAM(
                            "application/octet-stream"), APPLICATION_SVG_XML(
                                    "application/svg+xml"), APPLICATION_XHTML_XML(
                                            "application/xhtml+xml"), APPLICATION_XML(
                                                    "application/xml"),
    //    CHARSET_PARAMETER,
    MEDIA_TYPE_WILDCARD("*"), MULTIPART_FORM_DATA(
            "multipart/form-data"), TEXT_HTML("text/html"), TEXT_PLAIN(
                    "text/plain"), TEXT_XML("text/xml"), WILDCARD("*/*");
    //CHECKSTYLE.ON: Variable JavaDoc

    private final String mediaText;

    RESTMedias(final String media) {
        this.mediaText = media;
    }

    /**
     * Builds a {@link RESTMedias} instance (corresponding to
     * {@link MediaType}s) given its textual representation.
     * 
     * @param media
     *            The textual representation of the {@link RESTMedias} instance
     *            to build
     * @return The {@link RESTMedias} instance corresponding to the given
     *         textual representation
     * @throws UnknownMediaTypeException
     *             If the given textual representation does not correspond to
     *             any known {@link RESTMedias} instance (thus to any known
     *             {@link MediaType})
     */
    public static RESTMedias fromMediaText(final String media)
            throws UnknownMediaTypeException {
        if (media != null) {
            for (final RESTMedias rm : RESTMedias.values()) {
                if (media.equalsIgnoreCase(rm.mediaText)) {
                    return rm;
                }
            }
        }
        throw new UnknownMediaTypeException(
                String.format("Unknown REST media type <%s>", media));
    }

}
