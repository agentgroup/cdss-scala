package eu.connecare.cdss.rest;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.ws.rs.core.UriBuilder;

import org.apache.commons.validator.routines.InetAddressValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

/**
 * Created by {user} on 21 nov 2016 15:51:55
 */

/**
 * @author ste
 *
 */
public final class JerseySimpleServer {

	private static final Logger LOGGER = LogManager.getLogger(JerseySimpleServer.class);
	private static final String DEFAULT_PROPERTY_FILE = "src/main/resources/server-env.properties";
	private static final String ENDPOINT_IP = "endpoint.ip";
	private static final String ENDPOINT_TCP = "endpoint.tcp";
	private static final String RESOURCE_CLAZZ = "resource.clazz";
	private String endpointIp;
	private String adaptedIp;
	private int endpointTcp;
	private Class<?> resourceClazz;
	private HttpServer server;
	private static final int MIN_PORT = 1024;
	private static final int MAX_PORT = 65535;

	/**
	 * <p>
	 * Validates arguments as passed by a standard Java {@code main()} method.
	 * </p>
	 * <p>
	 * The method expects a single argument, which is interpreted as the
	 * filepath where the configuration file {@code *.properties} necessary for
	 * setting up {@code this} server can be found.
	 * </p>
	 * <p>
	 * If no arguments are given, the method falls back to the default
	 * configuration file {@code server-env.properties}.
	 * </p>
	 * <p>
	 * If more than one argument is given, any other argument is ignored.
	 * </p>
	 * 
	 * @param args
	 *            A single argument is expected: the filepath to a
	 *            {@code *.properties} configuration file
	 * @return The {@link String} representing the filepath actually considered
	 */
	public static String validateArgs(final String[] args) {
		switch (args.length) {
		case 0:
			JerseySimpleServer.LOGGER.warn(String.format("No properties file given, falling back to default <%s>",
					JerseySimpleServer.DEFAULT_PROPERTY_FILE));
			return JerseySimpleServer.DEFAULT_PROPERTY_FILE;
		case 1:
			JerseySimpleServer.LOGGER.info(String.format("Accepting user given property file <%s>", args[0]));
			return args[0];
		default:
			JerseySimpleServer.LOGGER.warn(String.format(
					"Too many arguments (usage: $> java JerseySimpleServer path/to/file.properties), considering only <%s> (first one)",
					args[0]));
			return args[0];
		}
	}

	/**
	 * Loads the {@code *.properties} file.
	 * 
	 * @param propsFile
	 *            The filepath where the {@code *.properties} can be found
	 * @return The {@link Properties} loaded
	 * @throws IOException
	 *             If the {@code *.properties} file cannot be found, read, or
	 *             any other I/O exception occurs
	 */
	public static Properties loadProps(final String propsFile) throws IOException {
		final Properties props = new Properties();
		props.load(Files.newInputStream(Paths.get(propsFile)));
		return props;
	}

	/**
	 * Parses the loaded {@link Properties} to initialise {@code this} server.
	 * 
	 * @param props
	 *            The {@link Properties} to parse
	 * @throws InvalidPropertiesFormatException
	 *             If the {@link Properties} are badly formatted, their keys
	 *             unknown, or their values invalid (e.g. non valid IP address
	 *             or reserved TCP port number)
	 */
	public void parseProps(final Properties props) throws InvalidPropertiesFormatException {
		this.endpointIp = props.getProperty(JerseySimpleServer.ENDPOINT_IP);
		final InetAddressValidator inetVal = InetAddressValidator.getInstance();
		if (this.endpointIp == null || this.endpointIp.isEmpty() || !inetVal.isValidInet4Address(this.endpointIp)) {
			throw new InvalidPropertiesFormatException(
					String.format("Given <%s> in props file <%s> is NOT valid", JerseySimpleServer.ENDPOINT_IP, props));
		}
		if ("127.0.0.1".equals(this.endpointIp)) {
			this.adaptedIp = "http://localhost/";
		}
		final String endTcp = props.getProperty(JerseySimpleServer.ENDPOINT_TCP);
		// FIXME Ugly, exception thrown is the same, can't re-wire conditional
		// logic to have single throw?
		if (endTcp != null && !endTcp.isEmpty()) {
			final int portno = Integer.parseInt(endTcp);
			if (portno > MIN_PORT && portno <= MAX_PORT) {
				this.endpointTcp = Integer.parseInt(endTcp);
			} else {
				throw new InvalidPropertiesFormatException(String.format("Given <%s> in props file <%s> is NOT valid",
						JerseySimpleServer.ENDPOINT_TCP, props));
			}
		} else {
			throw new InvalidPropertiesFormatException(String.format("Given <%s> in props file <%s> is NOT valid",
					JerseySimpleServer.ENDPOINT_TCP, props));
		}
		final String resClazz = props.getProperty(JerseySimpleServer.RESOURCE_CLAZZ);
		if (resClazz == null || resClazz.isEmpty()) {
			throw new InvalidPropertiesFormatException(String.format("Given <%s> in props file <%s> is NOT valid",
					JerseySimpleServer.RESOURCE_CLAZZ, props));
		}
		try {
			this.resourceClazz = Class.forName(resClazz);
		} catch (final ClassNotFoundException e) {
			final InvalidPropertiesFormatException ex = new InvalidPropertiesFormatException(String.format(
					"Given <%s> in props file <%s> is NOT on classpath", JerseySimpleServer.RESOURCE_CLAZZ, props));
			ex.initCause(e);
			throw ex;
		}
	}

	/**
	 * Deploys {@code this} server according to the configuration file given.
	 * 
	 * @return {@code this} server
	 * @throws IOException
	 *             If the underlying Grizzly container cannot start the server
	 *             for any reason
	 *
	 */
	public JerseySimpleServer deploy() throws IOException {
		final URI baseUri = UriBuilder.fromUri(this.adaptedIp).port(this.endpointTcp).build();
		final ResourceConfig config = new ResourceConfig(this.resourceClazz);
		this.server = GrizzlyHttpServerFactory.createHttpServer(baseUri, config);
		this.server.start();
		return this;
	}

	/**
	 * Gracefully shuts down {@code this} server.
	 * 
	 * @param gracePeriod
	 *            The period to gracefully wait for shutdown
	 * @param timeUnit
	 *            The {@link TimeUnit} according to which the
	 *            {@code gracePeriod} is expressed
	 * @return {@code this} server
	 */
	public JerseySimpleServer shutdown(final long gracePeriod, final TimeUnit timeUnit) {
		if (this.server != null && this.server.isStarted()) {
			this.server.shutdown(gracePeriod, timeUnit);
			JerseySimpleServer.LOGGER.info(
					String.format("Jersey RESTful endpoint on <%s:%s> exposing resource <%s> successfully stopped",
							this.adaptedIp, this.endpointTcp, this.getResourceClazz()));
		} else {
			JerseySimpleServer.LOGGER.warn(String.format("Jersey RESTful endpoint on <%s:%s> is already NOT active",
					this.adaptedIp, this.endpointTcp));
		}
		return this;
	}

	/**
	 * Checks whether {@code this} server is currently up&running or not.
	 * 
	 * @return {@code true} if {@code this} server is NOT up&running,
	 *         {@code false} otherwise
	 */
	public boolean isDown() {
		return !this.server.isStarted();
	}

	/**
	 * Exposes the IP address where {@code this} RESTful endpoint is available.
	 * 
	 * @return The IP address where {@code this} RESTful endpoint is available
	 */
	public String getEndpointIp() {
		return this.endpointIp;
	}

	/**
	 * Exposes the TCP port number where {@code this} RESTful endpoint is
	 * available.
	 * 
	 * @return The TCP port number where {@code this} RESTful endpoint is
	 *         available
	 */
	public int getEndpointTcp() {
		return this.endpointTcp;
	}

	/**
	 * <p>
	 * Exposes the "adapted IP" where {@code this} RESTful endpoint is
	 * available.
	 * </p>
	 * <p>
	 * The adapted IP is simply:
	 * <ul>
	 * <li>"http://localhost/" instead of "127.0.0.1", in case the configuration
	 * file {@code *.properties} specifies "127.0.0.1"
	 * <li>the same IP address specified in the configuration file
	 * {@code *.properties} otherwise
	 * </ul>
	 * </p>
	 * <p>
	 * This adaptation is a workaround for a testing issue.
	 * {@link InetAddressValidator} does not recognises "http://localhost/" as a
	 * valid IP address, but that's the address Jersey expects when working
	 * locally.
	 * </p>
	 * <p>
	 * Thus, the workaround is that in the configuration file
	 * {@code *.properties} we put "127.0.0.1", then we translate it in
	 * "http://localhost/" after validation, to succesfully configure
	 * {@code this} jersey server while still passing
	 * {@link InetAddressValidator} checks.
	 * </p>
	 * 
	 * @return The adapted IP, that is, "http://localhost/" in case the
	 *         configuration file {@code *.properties} specifies "127.0.0.1"
	 */
	public String getAdaptedIp() {
		return this.adaptedIp;
	}

	/**
	 * Exposes the {@link String} name of the REST resource (Java class)
	 * {@code this} RESTful endpoint makes available.
	 * 
	 * @return The {@link String} name of the REST resource (Java class)
	 *         {@code this} RESTful endpoint makes available
	 */
	public String getResourceClazz() {
		return this.resourceClazz.getName();
	}

}
