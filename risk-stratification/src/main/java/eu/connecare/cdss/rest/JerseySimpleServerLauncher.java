/**
 * Created by {user} on 28 nov 2016 08:15:05
 */
package eu.connecare.cdss.rest;

import java.io.IOException;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author ste
 *
 */
public final class JerseySimpleServerLauncher {

	private static final Logger LOGGER = LogManager.getLogger(JerseySimpleServerLauncher.class);

	private JerseySimpleServerLauncher() {
	}

	/**
	 * @param args
	 *            The filepath where the configuration file {@code *.properties}
	 *            necessary for setting up {@link JerseySimpleServer} can be
	 *            found.
	 */
	public static void main(final String[] args) {
		final JerseySimpleServer server = new JerseySimpleServer();
		final String propsFile = JerseySimpleServer.validateArgs(args);
		Properties props = null;
		try {
			props = JerseySimpleServer.loadProps(propsFile);
		} catch (final IOException e) {
			JerseySimpleServerLauncher.LOGGER
					.fatal(String.format("Cannot load given property file (<%s>) due to <%s>", propsFile, e));
			System.exit(-1);
		}
		try {
			server.parseProps(props);
		} catch (final InvalidPropertiesFormatException e) {
			JerseySimpleServerLauncher.LOGGER.fatal(String.format("Cannot parse properties due to <%s>", e));
			System.exit(-1);
		}
		try {
			server.deploy();
			JerseySimpleServerLauncher.LOGGER
					.info(String.format("Jersey RESTful endpoint up&running on <%s:%s> exposing resource <%s>",
							server.getAdaptedIp(), server.getEndpointTcp(), server.getResourceClazz()));
		} catch (final IOException e) {
			JerseySimpleServerLauncher.LOGGER.fatal(String.format("Cannot deploy server due to <%s>", e));
		}
	}

}
