package eu.connecare.cdss.rest;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.apache.commons.validator.routines.InetAddressValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by {user} on 21 nov 2016 17:30:25
 */

/**
 * @author ste
 *
 */
public final class JerseySimpleClient {

	private static final Logger LOGGER = LogManager.getLogger(JerseySimpleClient.class);
	private static final String DEFAULT_PROPERTY_FILE = "src/main/resources/client-env.properties";
	private static final String SERVER_IP = "server.ip";
	private static final String SERVER_TCP = "server.tcp";
	private static final String RESOURCE_REQ = "resource.req";
	private static final String RESOURCE_PATH = "resource.path";
	private static final String RESOURCE_ACCEPTS = "resource.accepts";
	private static final String REQ_ARG = "req.arg";
	private String serverIp;
	private String adaptedIp;
	private int serverTcp;
	private String resourceReq;
	private String resourcePath;
	private MediaType resourceAccepts;
	private Object reqArg;
	private static final int MIN_PORT = 1024;
	private static final int MAX_PORT = 65535;

	/**
	 * <p>
	 * Validates arguments as passed by a standard Java {@code main()} method.
	 * </p>
	 * <p>
	 * The method expects a single argument, which is interpreted as the
	 * filepath where the configuration file {@code *.properties} necessary for
	 * setting up {@code this} client can be found.
	 * </p>
	 * <p>
	 * If no arguments are given, the method falls back to the default
	 * configuration file {@code client-env.properties}.
	 * </p>
	 * <p>
	 * If more than one argument is given, any other argument is ignored.
	 * </p>
	 * 
	 * @param args
	 *            A single argument is expected: the filepath to a
	 *            {@code *.properties} configuration file
	 * @return The {@link String} representing the filepath actually considered
	 */
	public static String validateArgs(final String[] args) {
		switch (args.length) {
		case 0:
			JerseySimpleClient.LOGGER.warn(String.format("No properties file given, falling back to default <%s>",
					JerseySimpleClient.DEFAULT_PROPERTY_FILE));
			return JerseySimpleClient.DEFAULT_PROPERTY_FILE;
		case 1:
			JerseySimpleClient.LOGGER.info(String.format("Accepting user given property file <%s>", args[0]));
			return args[0];
		default:
			JerseySimpleClient.LOGGER.warn(String.format(
					"Too many arguments (usage: $> java JerseySimpleClient path/to/file.properties), considering only <%s> (first one)",
					args[0]));
			return args[0];
		}
	}

	/**
	 * Loads the {@code *.properties} file.
	 * 
	 * @param propsFile
	 *            The filepath where the {@code *.properties} can be found
	 * @return The {@link Properties} loaded
	 * @throws IOException
	 *             If the {@code *.properties} file cannot be found, read, or
	 *             any other I/O exception occurs
	 */
	public static Properties loadProps(final String propsFile) throws IOException {
		final Properties props = new Properties();
		props.load(Files.newInputStream(Paths.get(propsFile)));
		return props;
	}

	/**
	 * Parses the loaded {@link Properties} to initialise {@code this} client.
	 * 
	 * @param props
	 *            The {@link Properties} to parse
	 * @throws InvalidPropertiesFormatException
	 *             If the {@link Properties} are badly formatted, their keys
	 *             unknown, or their values invalid (e.g. non valid IP address
	 *             or reserved TCP port number)
	 */
	public void parseProps(final Properties props) throws InvalidPropertiesFormatException {
		this.serverIp = props.getProperty(JerseySimpleClient.SERVER_IP);
		final InetAddressValidator inetVal = InetAddressValidator.getInstance();
		if (this.serverIp == null || this.serverIp.isEmpty() || !inetVal.isValidInet4Address(this.serverIp)) {
			throw new InvalidPropertiesFormatException(
					String.format("Given <%s> in props file <%s> is NOT valid", JerseySimpleClient.SERVER_IP, props));

		}
		if ("127.0.0.1".equals(this.serverIp)) {
			this.adaptedIp = "http://localhost/";
		} else {
			this.adaptedIp = this.serverIp;
		}
		final String servTcp = props.getProperty(JerseySimpleClient.SERVER_TCP);
		// FIXME Ugly, exception thrown is the same, can't re-wire conditional
		// logic to have single throw?
		if (servTcp != null && !servTcp.isEmpty()) {
			final int portno = Integer.parseInt(servTcp);
			if (portno > MIN_PORT && portno <= MAX_PORT) {
				this.serverTcp = Integer.parseInt(servTcp);
			} else {
				throw new InvalidPropertiesFormatException(String.format("Given <%s> in props file <%s> is NOT valid",
						JerseySimpleClient.SERVER_TCP, props));
			}
		} else {
			throw new InvalidPropertiesFormatException(
					String.format("Given <%s> in props file <%s> is NOT valid", JerseySimpleClient.SERVER_TCP, props));
		}
		this.resourceReq = props.getProperty(JerseySimpleClient.RESOURCE_REQ);
		if (this.resourceReq == null || this.resourceReq.isEmpty()
				|| !JerseySimpleClient.validateReq(this.resourceReq)) {
			throw new InvalidPropertiesFormatException(String.format("Given <%s> in props file <%s> is NOT valid",
					JerseySimpleClient.RESOURCE_REQ, props));
		}
		this.resourcePath = props.getProperty(JerseySimpleClient.RESOURCE_PATH);
		if (this.resourcePath == null || this.resourcePath.isEmpty()) {
			throw new InvalidPropertiesFormatException(String.format("Given <%s> in props file <%s> is NOT valid",
					JerseySimpleClient.RESOURCE_PATH, props));
		}
		try {
			JerseySimpleClient.validatePath(this.resourcePath);
		} catch (final IOException e) {
			final InvalidPropertiesFormatException ex = new InvalidPropertiesFormatException(String
					.format("Given <%s> in props file <%s> is NOT valid", JerseySimpleClient.RESOURCE_PATH, props));
			ex.initCause(e);
			throw ex;
		}
		final String resAcc = props.getProperty(JerseySimpleClient.RESOURCE_ACCEPTS);
		if (resAcc == null || resAcc.isEmpty()) {
			throw new InvalidPropertiesFormatException(String.format("Given <%s> in props file <%s> is NOT valid",
					JerseySimpleClient.RESOURCE_ACCEPTS, props));
		}
		try {
			JerseySimpleClient.validateMedia(resAcc);
		} catch (final UnknownMediaTypeException e) {
			final InvalidPropertiesFormatException ex = new InvalidPropertiesFormatException(String
					.format("Given <%s> in props file <%s> is NOT valid", JerseySimpleClient.RESOURCE_ACCEPTS, props));
			ex.initCause(e);
			throw ex;
		}
		this.resourceAccepts = MediaType.valueOf(resAcc);
		// FIXME How to render this type-agnostic? Also, argument is only
		// necessary for a POST or PUT
		if (RESTReqs.valueOf(this.resourceReq) == RESTReqs.POST || RESTReqs.valueOf(this.resourceReq) == RESTReqs.PUT) {
			final String requestArg = props.getProperty(JerseySimpleClient.REQ_ARG);
			if (requestArg == null) {
				throw new InvalidPropertiesFormatException(
						String.format("Given <%s> in props file <%s> is NOT valid", JerseySimpleClient.REQ_ARG, props));
			}
			this.reqArg = requestArg;
		}
	}

	private static void validatePath(final String path) throws IOException {
		new File(path).getCanonicalPath();
	}

	private static void validateMedia(final String media) throws UnknownMediaTypeException {
		Arrays.asList(RESTMedias.values()).contains(RESTMedias.fromMediaText(media));
	}

	private static boolean validateReq(final String req) {
		if (Arrays.asList(RESTReqs.values()).contains(RESTReqs.valueOf(req))) {
			return true;
		}
		return false;
	}

	/**
	 * Initialises the {@link WebTarget} {@code this} client targets based on
	 * the IP address and TCP port number parsed from the {@code *.properties}
	 * file.
	 * 
	 * @return The {@link WebTarget} {@code this} client should contact
	 */
	public WebTarget initBaseTarget() {
		final URI baseUri = UriBuilder.fromUri(this.adaptedIp).port(this.serverTcp).build();
		final Client client = ClientBuilder.newClient();
		return client.target(baseUri);
	}

	/**
	 * Configures a {@link Builder} for building an {@link Invocation} for a
	 * resource available at the given RESTful path and accepting the given
	 * {@link MediaType}.
	 * 
	 * @param baseTarget
	 *            The {@link WebTarget}exposing the resource
	 * @param path
	 *            The RESTful path where the resource is available
	 * @param media
	 *            The {@link MediaType} the resource accepts
	 * @return The {@link Builder} configured
	 */
	public static Builder buildMinimalRequest(final WebTarget baseTarget, final String path, final MediaType media) {
		return baseTarget.path(path).request(media);
	}

	/**
	 * Executes a GET request based on the given pre-configured
	 * {@link Invocation.Builder}.
	 * 
	 * @param req
	 *            The pre-configured {@link Invocation.Builder}
	 * @return The {@link Response} obtained
	 */
	public static Response doGet(final Invocation.Builder req) {
		final Response response;
		response = req.get();
		return response;
	}

	/**
	 * Executes a POST request based on the given pre-configured
	 * {@link Invocation.Builder}.
	 * 
	 * @param req
	 *            The pre-configured {@link Invocation.Builder}
	 * @return The {@link Response} obtained
	 */
	public Response doPost(final Invocation.Builder req) {
		Response response;
		// FIXME How to render this type-agnostic?
		response = req.post(Entity.text(this.reqArg));
		return response;
	}

	/**
	 * <p>
	 * Exposes the "adapted IP" of the RESTful endpoint to contact.
	 * </p>
	 * <p>
	 * The adapted IP is simply:
	 * <ul>
	 * <li>"http://localhost/" instead of "127.0.0.1", in case the configuration
	 * file {@code *.properties} specifies "127.0.0.1"
	 * <li>the same IP address specified in the configuration file
	 * {@code *.properties} otherwise
	 * </ul>
	 * </p>
	 * <p>
	 * This adaptation is a workaround for a testing issue.
	 * {@link InetAddressValidator} does not recognises "http://localhost/" as a
	 * valid IP address, but that's the address Jersey expects when working
	 * locally.
	 * </p>
	 * <p>
	 * Thus, the workaround is that in the configuration file
	 * {@code *.properties} we put "127.0.0.1", then we translate it in
	 * "http://localhost/" after validation, to succesfully configure
	 * {@code this} jersey client while still passing
	 * {@link InetAddressValidator} checks.
	 * </p>
	 * 
	 * @return The adapted IP, that is, "http://localhost/" in case the
	 *         configuration file {@code *.properties} specifies "127.0.0.1"
	 */
	public String getAdaptedIp() {
		return this.adaptedIp;
	}

	/**
	 * Exposes the IP address of the RESTful endpoint to contact.
	 * 
	 * @return The IP address of the RESTful endpoint to contact
	 */
	public String getServerIp() {
		return this.serverIp;
	}

	/**
	 * Exposes the TCP port number of the RESTful endpoint to contact.
	 * 
	 * @return The TCP port number of the RESTful endpoint to contact
	 */
	public int getServerTcp() {
		return this.serverTcp;
	}

	/**
	 * Exposes the REST request {@code this} client is configured to issue.
	 * 
	 * @return The REST request {@code this} client is configured to issue
	 */
	public String getResourceReq() {
		return this.resourceReq;
	}

	/**
	 * Exposes the RESTful path where the resource {@code this} client is
	 * configured to contact is available.
	 * 
	 * @return The RESTful path where the resource {@code this} client is
	 *         configured to contact is available
	 */
	public String getResourcePath() {
		return this.resourcePath;
	}

	/**
	 * Exposes the {@link MediaType} that the resource {@code this} client is
	 * configured to contact accepts.
	 * 
	 * @return The {@link MediaType} that the resource {@code this} client is
	 *         configured to contact accepts
	 */
	public MediaType getResourceAccepts() {
		return this.resourceAccepts;
	}

	/**
	 * <p>
	 * Exposes the argument of a POST or PUT REST request.
	 * </p>
	 * <p>
	 * In case {@code this} client is NOT configured for a POST or PUT, the
	 * method may return {@code null} or whatever unused argument the
	 * configuration file specifies.
	 * </p>
	 * 
	 * @return The {@link MediaType} that the resource {@code this} client is
	 *         configured to contact accepts
	 */
	public Object getReqArg() {
		return this.reqArg;
	}
}
