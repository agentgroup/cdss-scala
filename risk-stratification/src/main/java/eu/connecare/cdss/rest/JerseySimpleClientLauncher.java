/**
 * Created by {user} on 28 nov 2016 17:29:24
 */
package eu.connecare.cdss.rest;

import java.io.IOException;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;

import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author ste
 *
 */
public final class JerseySimpleClientLauncher {

	private static final Logger LOGGER = LogManager.getLogger(JerseySimpleClientLauncher.class);

	private JerseySimpleClientLauncher() {
	}

	/**
	 * @param args
	 *            The filepath where the configuration file {@code *.properties}
	 *            necessary for setting up {@link JerseySimpleClient} can be
	 *            found.
	 */
	public static void main(final String[] args) {
		final JerseySimpleClient client = new JerseySimpleClient();
		final String propsFile = JerseySimpleClient.validateArgs(args);
		Properties props = null;
		try {
			props = JerseySimpleClient.loadProps(propsFile);
		} catch (final IOException e) {
			JerseySimpleClientLauncher.LOGGER
					.fatal(String.format("Cannot load given property file (<%s>) due to <%s>", propsFile, e));
			System.exit(-1);
		}
		try {
			client.parseProps(props);
		} catch (final InvalidPropertiesFormatException e) {
			JerseySimpleClientLauncher.LOGGER.fatal(String.format("Cannot parse properties due to <%s>", e));
			System.exit(-1);
		}
		final WebTarget baseTarget = client.initBaseTarget();
		JerseySimpleClientLauncher.LOGGER
				.info(String.format("Jersey RESTful client configured for <%s:%s> as base target",
						client.getAdaptedIp(), client.getServerTcp()));
		final Invocation.Builder req = JerseySimpleClient.buildMinimalRequest(baseTarget, client.getResourcePath(),
				client.getResourceAccepts());
		Response response = null;
		switch (client.getResourceReq()) {
		case "GET":
			JerseySimpleClientLauncher.LOGGER.info(String.format("Invoking HTTP GET on <%s> accepting <%s>",
					client.getResourcePath(), client.getResourceAccepts()));
			response = JerseySimpleClient.doGet(req);
			break;
		case "POST":
			JerseySimpleClientLauncher.LOGGER
					.info(String.format("Invoking HTTP POST on <%s> accepting <%s> with payload <%s>",
							client.getResourcePath(), client.getResourceAccepts(), client.getReqArg()));
			response = client.doPost(req);
			break;
		default:
			JerseySimpleClientLauncher.LOGGER.warn("HTTP request not supported atm");
			break;
		}
		if (response != null) {
			JerseySimpleClientLauncher.LOGGER.info(String.format("Got HTTP status <%s> with payload <%s>",
					response.getStatus(), response.readEntity(String.class)));
		}
	}

}
