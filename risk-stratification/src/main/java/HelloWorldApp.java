import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.Application;

/**
 * Created by {user} on 21 nov 2016 15:58:54
 */

/**
 * @author ste ASK AFAIK this is an alternative to {@code ResourceConfig} used
 *         in {@code JerseySimpleServer}
 */
public class HelloWorldApp extends Application {

	/**
	 * @return
	 * @see javax.ws.rs.core.Application#getClasses()
	 */
	@Override
	public Set<Class<?>> getClasses() {
		final Set<Class<?>> s = new HashSet<>();
		s.add(CustomHelloWorldResource.class);
		return s;
	}

}
