/**
 * Created by {user} on 29 nov 2016 15:46:16
 */
package eu.connecare.cdss.rest;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.InvalidPropertiesFormatException;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.assertj.core.api.Assertions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 * @author ste
 *
 */
@RunWith(Parameterized.class)
public class JerseyClientServerInteractionTest {

	private static final Logger LOGGER = LogManager.getLogger(JerseyClientServerInteractionTest.class);
	private static final String DEFAULT_CONGIF_FILE = "src/test/resources/interaction-test.config";
	private JerseySimpleServer server;
	private JerseySimpleClient client;
	private final String propsFile;
	private final String expected;

	/**
	 * <p>
	 * Test method for Jersey client/server interaction.
	 * </p>
	 *
	 * <p>
	 * A {@link JerseySimpleServer} RESTful endpoint is deployed and a
	 * {@link JerseySimpleClient} is configured to interact with such an
	 * endpoint.
	 * </p>
	 *
	 * <p>
	 * Both entities are configured based on
	 * {@code interaction-env-*.properties} files, each storing a different
	 * configuration to test.
	 * </p>
	 *
	 * <p>
	 * The {@code interaction-env-*.properties} files to consider as tests
	 * parameters, and the expected result for each configuration, are defined
	 * in method {@link JerseyClientServerInteractionTest#interactionConfigs()}.
	 * </p>
	 *
	 * @param propertiesFilePath
	 *            the filepath where the {@code *.properties} file can be found
	 * @param expectedResponse
	 *            the {@link String} representation of the expected REST
	 *            {@link Response} obtained from the contacted endpoint
	 */
	public JerseyClientServerInteractionTest(final String propertiesFilePath, final String expectedResponse) {
		this.propsFile = propertiesFilePath;
		this.expected = expectedResponse;
	}

	/**
	 * <p>
	 * Defines the {@code interaction-env-*.properties} files to consider as
	 * tests parameters, and the expected result for each configuration.
	 * </p>
	 *
	 * @return The {@link Collection} of test configuration – expected result
	 *         pairs to feed test
	 *         {@link JerseyClientServerInteractionTest#test()}
	 */
	@Parameterized.Parameters
	public static Collection<?> interactionConfigs() {
		List<String> lines = null;
		try {
			lines = Files.lines(Paths.get(JerseyClientServerInteractionTest.DEFAULT_CONGIF_FILE))
					.collect(Collectors.toList());
		} catch (final IOException e) {
			Assertions.fail(String.format("Test cannot parse configuration file <%s> due to <%s>",
					JerseyClientServerInteractionTest.DEFAULT_CONGIF_FILE, e));
		}
		final List<String[]> splitLines = new ArrayList<>();
		for (final String line : lines) {
			splitLines.add(line.split("="));
		}
		/*
		 * Always 2 due to structure of interaction-test.config file
		 */
		final String[][] configs = new String[splitLines.size()][2];
		int row = 0;
		for (final String[] splitLine : splitLines) {
			configs[row][0] = splitLine[0].trim();
			configs[row][1] = splitLine[1].trim();
			/*
			 * See comment above
			 */
			JerseyClientServerInteractionTest.LOGGER
					.info(String.format("Configuration %s is: %s, %s", row, configs[row][0], configs[row][1]));
			row++;
		}
		return Arrays.asList(configs);
	}

	// CHECKSTYLE.OFF: Method Javadoc
	@Before
	public void setUp() {
		this.server = new JerseySimpleServer();
		this.client = new JerseySimpleClient();
	}

	@After
	public void tearDown() {
		this.server = null;
		this.client = null;
	}
	// CHECKSTYLE.OFF: Method Javadoc

	/**
	 * Tests an interaction scenario between {@link JerseySimpleClient} and
	 * {@link JerseySimpleServer} parametrised according to the
	 * {@code interaction-env-*.properties} files and expected results defined
	 * in method {@link JerseyClientServerInteractionTest#interactionConfigs()}.
	 */
	@Test
	public final void test() {
		try {
			this.server.parseProps(JerseySimpleServer.loadProps(this.propsFile));
		} catch (final InvalidPropertiesFormatException e) {
			Assertions.fail(String.format("Server cannot parse properties due to <%s>", e));
		} catch (final IOException e) {
			Assertions.fail(String.format("Server cannot load properties due to <%s>", e));
		}
		try {
			this.server.deploy();
			JerseyClientServerInteractionTest.LOGGER
					.info(String.format("Jersey RESTful endpoint up&running on <%s:%s> exposing resource <%s>",
							this.server.getAdaptedIp(), this.server.getEndpointTcp(), this.server.getResourceClazz()));
		} catch (final IOException e) {
			Assertions.fail(String.format("Cannot deploy server due to <%s>", e));
		}
		try {
			this.client.parseProps(JerseySimpleClient.loadProps(this.propsFile));
		} catch (final InvalidPropertiesFormatException e) {
			Assertions.fail(String.format("Client cannot parse properties due to <%s>", e));
		} catch (final IOException e) {
			Assertions.fail(String.format("Client cannot load properties due to <%s>", e));
		}
		JerseyClientServerInteractionTest.LOGGER.info(String.format("Invoking HTTP %s on <%s> accepting <%s>",
				this.client.getResourceReq(), this.client.getResourcePath(), this.client.getResourceAccepts()));
		Response response = null;
		switch (RESTReqs.valueOf(this.client.getResourceReq())) {
		case GET:
			response = JerseySimpleClient.doGet(JerseySimpleClient.buildMinimalRequest(this.client.initBaseTarget(),
					this.client.getResourcePath(), this.client.getResourceAccepts()));
			break;
		case POST:
			response = this.client.doPost(JerseySimpleClient.buildMinimalRequest(this.client.initBaseTarget(),
					this.client.getResourcePath(), this.client.getResourceAccepts()));
			break;
		default:
			JerseyClientServerInteractionTest.LOGGER
					.fatal(String.format("Unsupported HTTP request (%s)", this.client.getResourceReq()));
			Assertions.fail(String.format("Unsupported HTTP request (%s)", this.client.getResourceReq()));
			break;
		}
		if (response != null) {
			final int status = response.getStatus();
			final String readEntity = response.readEntity(String.class);
			JerseyClientServerInteractionTest.LOGGER
					.info(String.format("Got HTTP status <%s> with payload <%s>", status, readEntity));
			// CHECKSTYLE.OFF: Magic Number
			Assertions.assertThat(status).isEqualTo(200); // HTTP Status Code:
															// OK
			// CHECKSTYLE.ON: Magic Number
			Assertions.assertThat(readEntity).isEqualTo(this.expected);
		} else {
			Assertions.fail("Response is <null>");
		}
		this.server.shutdown(3, TimeUnit.SECONDS);
		Assertions.assertThat(this.server.isDown());
	}

}
