/**
 * Created by {user} on 28 nov 2016 18:04:53
 */
package eu.connecare.cdss.rest;

import java.io.IOException;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.assertj.core.api.Assertions;
import org.assertj.core.api.SoftAssertions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author ste
 *
 */
public class JerseySimpleClientTest {

    private static final String[] ARGS_0 = new String[] {};
    private static final String[] ARGS_1 = new String[] {
            "path/to/file.properties" };
    private static final String[] ARGS_2 = new String[] {
            "path/to/file.properties", "stuff/nobody/cares.about" };
    private static final String DEFAULT_PROPERTY_FILE = "src/main/resources/client-env.properties";
    private static final String USER_PROPERTY_FILE = "path/to/file.properties";
    private static Properties defaultProperties;
    private static Properties invalidPropertiesKeys;
    private static Properties invalidPropertiesValues;
    private static JerseySimpleClient client;

    //CHECKSTYLE.OFF: Method Javadoc
    @SuppressWarnings("static-method") // JUnit requires non-static methods
    @Before
    public void setUp() {
        JerseySimpleClientTest.client = new JerseySimpleClient();
        Assertions.assertThat(JerseySimpleClientTest.client).isNotNull();
        JerseySimpleClientTest.defaultProperties = new Properties();
        JerseySimpleClientTest.defaultProperties.put("server.ip", "127.0.0.1");
        JerseySimpleClientTest.defaultProperties.put("server.tcp", "9998");
        JerseySimpleClientTest.defaultProperties.put("resource.req", "GET");
        JerseySimpleClientTest.defaultProperties.put("resource.path",
                "/test/ste");
        JerseySimpleClientTest.defaultProperties.put("resource.accepts",
                "text/plain");
        JerseySimpleClientTest.defaultProperties.put("req.arg", "TEST");
        Assertions.assertThat(JerseySimpleClientTest.defaultProperties)
                .isNotNull();
        JerseySimpleClientTest.invalidPropertiesKeys = new Properties();
        JerseySimpleClientTest.invalidPropertiesKeys
                .putAll(JerseySimpleClientTest.defaultProperties);
        Assertions.assertThat(JerseySimpleClientTest.invalidPropertiesKeys)
                .isNotNull();
        JerseySimpleClientTest.invalidPropertiesValues = new Properties();
        JerseySimpleClientTest.invalidPropertiesValues
                .putAll(JerseySimpleClientTest.defaultProperties);
        Assertions.assertThat(JerseySimpleClientTest.invalidPropertiesValues)
                .isNotNull();
    }

    @SuppressWarnings("static-method") // JUnit requires non-static methods
    @After
    public void tearDown() {
        JerseySimpleClientTest.client = null;
        JerseySimpleClientTest.defaultProperties = null;
        JerseySimpleClientTest.invalidPropertiesKeys = null;
        JerseySimpleClientTest.invalidPropertiesValues = null;
        Assertions.assertThat(JerseySimpleClientTest.client).isNull();
        Assertions.assertThat(JerseySimpleClientTest.defaultProperties)
                .isNull();
        Assertions.assertThat(JerseySimpleClientTest.invalidPropertiesKeys)
                .isNull();
        Assertions.assertThat(JerseySimpleClientTest.invalidPropertiesValues)
                .isNull();
    }
    //CHECKSTYLE.ON: Method Javadoc

    /**
     * Test method for
     * {@link eu.connecare.unimore.dss.RESTwrapper.JerseySimpleClient#validateArgs(java.lang.String[])}.
     *
     */
    @SuppressWarnings("static-method") // JUnit requires non-static methods
    @Test
    public final void testValidateArgs() {
        final SoftAssertions softly = new SoftAssertions();
        softly.assertThat(
                JerseySimpleClient.validateArgs(JerseySimpleClientTest.ARGS_0))
                .isEqualTo(JerseySimpleClientTest.DEFAULT_PROPERTY_FILE);
        softly.assertThat(
                JerseySimpleClient.validateArgs(JerseySimpleClientTest.ARGS_1))
                .isEqualTo(JerseySimpleClientTest.USER_PROPERTY_FILE);
        softly.assertThat(
                JerseySimpleClient.validateArgs(JerseySimpleClientTest.ARGS_2))
                .isEqualTo(JerseySimpleClientTest.USER_PROPERTY_FILE);
        softly.assertThat(
                JerseySimpleClient.validateArgs(JerseySimpleClientTest.ARGS_0))
                .isNotEqualTo(JerseySimpleClient
                        .validateArgs(JerseySimpleClientTest.ARGS_1));
        softly.assertThat(
                JerseySimpleClient.validateArgs(JerseySimpleClientTest.ARGS_0))
                .isNotEqualTo(JerseySimpleClient
                        .validateArgs(JerseySimpleClientTest.ARGS_2));
        softly.assertThat(
                JerseySimpleClient.validateArgs(JerseySimpleClientTest.ARGS_1))
                .isEqualTo(JerseySimpleClient
                        .validateArgs(JerseySimpleClientTest.ARGS_2));
        softly.assertAll();
    }

    /**
     * Test method for
     * {@link eu.connecare.unimore.dss.RESTwrapper.JerseySimpleClient#loadProps(java.lang.String)}.
     *
     */
    @SuppressWarnings("static-method") // JUnit requires non-static methods
    @Test
    public final void testLoadProps() {
        final SoftAssertions softly = new SoftAssertions();
        try {
            softly.assertThat(JerseySimpleClient
                    .loadProps(JerseySimpleClientTest.DEFAULT_PROPERTY_FILE))
                    .isEqualTo(JerseySimpleClientTest.defaultProperties);
        } catch (final IOException e) {
            e.printStackTrace();
        }
        softly.assertThatThrownBy(() -> {
            JerseySimpleClient.loadProps("lol/i/do/not/exist.fake");
        }).isInstanceOf(IOException.class);
        softly.assertAll();
    }

    /**
     * Test method for
     * {@link eu.connecare.unimore.dss.RESTwrapper.JerseySimpleClient#parseProps(java.util.Properties)}.
     *
     */
    @SuppressWarnings("static-method") // JUnit requires non-static methods
    @Test
    public final void testParseProps() {
        final SoftAssertions softly = new SoftAssertions();
        try {
            JerseySimpleClientTest.client
                    .parseProps(JerseySimpleClientTest.defaultProperties);
        } catch (final InvalidPropertiesFormatException e) {
            e.printStackTrace();
        }
        softly.assertThat(JerseySimpleClientTest.client.getServerIp())
                .isEqualTo(JerseySimpleClientTest.defaultProperties
                        .get("server.ip"));
        softly.assertThat(JerseySimpleClientTest.client.getServerTcp())
                .isEqualTo(
                        Integer.valueOf(JerseySimpleClientTest.defaultProperties
                                .get("server.tcp").toString()));
        softly.assertThat(JerseySimpleClientTest.client.getResourceReq())
                .isEqualTo(JerseySimpleClientTest.defaultProperties
                        .get("resource.req"));
        softly.assertThat(JerseySimpleClientTest.client.getResourcePath())
                .isEqualTo(JerseySimpleClientTest.defaultProperties
                        .get("resource.path"));
        softly.assertThat(JerseySimpleClientTest.client.getResourceAccepts())
                .isEqualTo(MediaType
                        .valueOf(JerseySimpleClientTest.defaultProperties
                                .get("resource.accepts").toString()));
        softly.assertThat(JerseySimpleClientTest.client.getReqArg()).isNull();
        JerseySimpleClientTest.defaultProperties.put("resource.accepts",
                "TEXT/PLAIN");
        try {
            JerseySimpleClientTest.client
                    .parseProps(JerseySimpleClientTest.defaultProperties);
        } catch (final InvalidPropertiesFormatException e) {
            e.printStackTrace();
        }
        softly.assertThat(JerseySimpleClientTest.client.getResourceAccepts())
                .isEqualTo(MediaType
                        .valueOf(JerseySimpleClientTest.defaultProperties
                                .get("resource.accepts").toString()));
        JerseySimpleClientTest.invalidPropertiesKeys.remove("server.ip");
        JerseySimpleClientTest.invalidPropertiesKeys.put("server ip",
                "127.0.0.1");
        softly.assertThatThrownBy(() -> {
            JerseySimpleClientTest.client
                    .parseProps(JerseySimpleClientTest.invalidPropertiesKeys);
        }).isInstanceOf(InvalidPropertiesFormatException.class)
                .hasMessageContaining("server.ip");
        JerseySimpleClientTest.invalidPropertiesKeys.clear();
        JerseySimpleClientTest.invalidPropertiesKeys
                .putAll(JerseySimpleClientTest.defaultProperties);
        JerseySimpleClientTest.invalidPropertiesKeys.remove("server.tcp");
        JerseySimpleClientTest.invalidPropertiesKeys.put(" server.tcp ",
                "9998");
        softly.assertThatThrownBy(() -> {
            JerseySimpleClientTest.client
                    .parseProps(JerseySimpleClientTest.invalidPropertiesKeys);
        }).isInstanceOf(InvalidPropertiesFormatException.class)
                .hasMessageContaining("server.tcp");
        JerseySimpleClientTest.invalidPropertiesKeys.clear();
        JerseySimpleClientTest.invalidPropertiesKeys
                .putAll(JerseySimpleClientTest.defaultProperties);
        JerseySimpleClientTest.invalidPropertiesKeys.remove("resource.req");
        JerseySimpleClientTest.invalidPropertiesKeys.put("resource\rreq",
                "GET");
        softly.assertThatThrownBy(() -> {
            JerseySimpleClientTest.client
                    .parseProps(JerseySimpleClientTest.invalidPropertiesKeys);
        }).isInstanceOf(InvalidPropertiesFormatException.class)
                .hasMessageContaining("resource.req");
        JerseySimpleClientTest.invalidPropertiesKeys.clear();
        JerseySimpleClientTest.invalidPropertiesKeys
                .putAll(JerseySimpleClientTest.defaultProperties);
        JerseySimpleClientTest.invalidPropertiesKeys.remove("resource.path");
        JerseySimpleClientTest.invalidPropertiesKeys.put("resource\\.path",
                "GET");
        softly.assertThatThrownBy(() -> {
            JerseySimpleClientTest.client
                    .parseProps(JerseySimpleClientTest.invalidPropertiesKeys);
        }).isInstanceOf(InvalidPropertiesFormatException.class)
                .hasMessageContaining("resource.path");
        JerseySimpleClientTest.invalidPropertiesKeys.clear();
        JerseySimpleClientTest.invalidPropertiesKeys
                .putAll(JerseySimpleClientTest.defaultProperties);
        JerseySimpleClientTest.invalidPropertiesKeys.remove("resource.path");
        JerseySimpleClientTest.invalidPropertiesKeys.put("resource\"path",
                "GET");
        softly.assertThatThrownBy(() -> {
            JerseySimpleClientTest.client
                    .parseProps(JerseySimpleClientTest.invalidPropertiesKeys);
        }).isInstanceOf(InvalidPropertiesFormatException.class)
                .hasMessageContaining("resource.path");
        JerseySimpleClientTest.invalidPropertiesKeys.clear();
        JerseySimpleClientTest.invalidPropertiesKeys
                .putAll(JerseySimpleClientTest.defaultProperties);
        JerseySimpleClientTest.invalidPropertiesKeys.put("resource.req",
                "POST");
        JerseySimpleClientTest.invalidPropertiesKeys.remove("req.arg");
        JerseySimpleClientTest.invalidPropertiesKeys.put("req\farg", "GET");
        softly.assertThatThrownBy(() -> {
            JerseySimpleClientTest.client
                    .parseProps(JerseySimpleClientTest.invalidPropertiesKeys);
        }).isInstanceOf(InvalidPropertiesFormatException.class)
                .hasMessageContaining("req.arg");
        JerseySimpleClientTest.invalidPropertiesValues.put("server.ip",
                "http://localhost/");
        softly.assertThatThrownBy(() -> {
            JerseySimpleClientTest.client
                    .parseProps(JerseySimpleClientTest.invalidPropertiesValues);
        }).isInstanceOf(InvalidPropertiesFormatException.class)
                .hasMessageContaining("server.ip");
        JerseySimpleClientTest.invalidPropertiesValues.put("server.tcp",
                "99498z");
        softly.assertThatThrownBy(() -> {
            JerseySimpleClientTest.client
                    .parseProps(JerseySimpleClientTest.invalidPropertiesValues);
        }).isInstanceOf(InvalidPropertiesFormatException.class)
                .hasMessageContaining("server.tcp");
        JerseySimpleClientTest.invalidPropertiesValues.put("server.tcp",
                "1024");
        softly.assertThatThrownBy(() -> {
            JerseySimpleClientTest.client
                    .parseProps(JerseySimpleClientTest.invalidPropertiesValues);
        }).isInstanceOf(InvalidPropertiesFormatException.class)
                .hasMessageContaining("server.tcp");
        JerseySimpleClientTest.invalidPropertiesValues.put("server.tcp",
                "65537");
        softly.assertThatThrownBy(() -> {
            JerseySimpleClientTest.client
                    .parseProps(JerseySimpleClientTest.invalidPropertiesValues);
        }).isInstanceOf(InvalidPropertiesFormatException.class)
                .hasMessageContaining("server.tcp");
        JerseySimpleClientTest.invalidPropertiesValues.put("resource.req",
                "UPDATE");
        softly.assertThatThrownBy(() -> {
            JerseySimpleClientTest.client
                    .parseProps(JerseySimpleClientTest.invalidPropertiesValues);
        }).isInstanceOf(InvalidPropertiesFormatException.class)
                .hasMessageContaining("resource.req");
        JerseySimpleClientTest.invalidPropertiesValues.put("resource.path",
                "'$e");
        softly.assertThatThrownBy(() -> {
            JerseySimpleClientTest.client
                    .parseProps(JerseySimpleClientTest.invalidPropertiesValues);
        }).isInstanceOf(InvalidPropertiesFormatException.class)
                .hasMessageContaining("resource.path");
        //              .hasCauseInstanceOf(IOException.class); // FIXME Not working: cause always <null>
        JerseySimpleClientTest.invalidPropertiesValues.put("resource.accepts",
                "text/fake");
        softly.assertThatThrownBy(() -> {
            JerseySimpleClientTest.client
                    .parseProps(JerseySimpleClientTest.invalidPropertiesValues);
        }).isInstanceOf(InvalidPropertiesFormatException.class)
                .hasMessageContaining("resource.accepts");
        //              .hasCauseInstanceOf(UnknownMediaTypeException.class); // FIXME Not working: cause always <null>
        softly.assertAll();
    }

    /**
     * Test method for
     * {@link eu.connecare.unimore.dss.RESTwrapper.JerseySimpleClient#initBaseTarget()}.
     *
     */
    @SuppressWarnings("static-method") // JUnit requires non-static methods
    @Test
    public final void testInitBaseTarget() {
        final SoftAssertions softly = new SoftAssertions();
        try {
            JerseySimpleClientTest.client
                    .parseProps(JerseySimpleClientTest.defaultProperties);
        } catch (final InvalidPropertiesFormatException e) {
            e.printStackTrace();
        }
        final WebTarget expected = ClientBuilder.newClient().target(UriBuilder
                .fromUri("http://localhost/")
                .port(Integer.valueOf(JerseySimpleClientTest.defaultProperties
                        .get("server.tcp").toString()))
                .build());
        final WebTarget actual = JerseySimpleClientTest.client.initBaseTarget();
        softly.assertThat(actual.getUri()).isEqualTo(expected.getUri());
        softly.assertThat(actual.toString()).isEqualTo(expected.toString());
        softly.assertThat(actual.getConfiguration().getProperties())
                .isEqualTo(expected.getConfiguration().getProperties());
        softly.assertThat(actual).isNotEqualTo(expected);
        softly.assertAll();
    }

    /**
     * Test method for
     * {@link eu.connecare.unimore.dss.RESTwrapper.JerseySimpleClient#buildMinimalRequest(javax.ws.rs.client.WebTarget, java.lang.String, javax.ws.rs.core.MediaType)}.
     *
     */
    @SuppressWarnings("static-method") // JUnit requires non-static methods
    @Test
    public final void testBuildMinimalRequest() {
        final SoftAssertions softly = new SoftAssertions();
        try {
            JerseySimpleClientTest.client
                    .parseProps(JerseySimpleClientTest.defaultProperties);
        } catch (final InvalidPropertiesFormatException e) {
            e.printStackTrace();
        }
        final WebTarget baseTarget = JerseySimpleClientTest.client
                .initBaseTarget();
        final Builder expected = baseTarget
                .path(JerseySimpleClientTest.defaultProperties
                        .getProperty("resource.path"))
                .request(JerseySimpleClientTest.defaultProperties
                        .getProperty("resource.accepts"));
        final Builder actual = JerseySimpleClient.buildMinimalRequest(
                baseTarget, JerseySimpleClientTest.client.getResourcePath(),
                JerseySimpleClientTest.client.getResourceAccepts());
        // TODO No equality test seems possible. Periodically check if new releases change this
        softly.assertThat(actual).isNotEqualTo(expected);
        softly.assertAll();
    }

}
