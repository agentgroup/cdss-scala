/**
 * Created by {user} on 25 nov 2016 15:07:00
 */
package eu.connecare.cdss.rest;

import java.io.IOException;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.assertj.core.api.Assertions;
import org.assertj.core.api.SoftAssertions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author ste
 *
 */
public class JerseySimpleServerTest {

    private static final String[] ARGS_0 = new String[] {};
    private static final String[] ARGS_1 = new String[] {
            "path/to/file.properties" };
    private static final String[] ARGS_2 = new String[] {
            "path/to/file.properties", "stuff/nobody/cares.about" };
    private static final String DEFAULT_PROPERTY_FILE = "src/main/resources/server-env.properties";
    private static final String USER_PROPERTY_FILE = "path/to/file.properties";
    private static Properties defaultProperties;
    private static Properties invalidPropertiesKeys;
    private static Properties invalidPropertiesValues;
    private static Properties invalidPropertiesClazz;
    private static JerseySimpleServer server;

    //CHECKSTYLE.OFF: Method Javadoc
    @SuppressWarnings("static-method") // JUnit requires non-static methods
    @Before
    public void setUp() {
        JerseySimpleServerTest.server = new JerseySimpleServer();
        Assertions.assertThat(JerseySimpleServerTest.server).isNotNull();
        JerseySimpleServerTest.defaultProperties = new Properties();
        JerseySimpleServerTest.defaultProperties.put("endpoint.ip",
                "127.0.0.1");
        JerseySimpleServerTest.defaultProperties.put("endpoint.tcp", "9998");
        JerseySimpleServerTest.defaultProperties.put("resource.clazz",
                "eu.connecare.cdss.rest.HelloWorldResource");
        Assertions.assertThat(JerseySimpleServerTest.defaultProperties)
                .isNotNull();
        JerseySimpleServerTest.invalidPropertiesKeys = new Properties();
        JerseySimpleServerTest.invalidPropertiesKeys
                .putAll(JerseySimpleServerTest.defaultProperties);
        Assertions.assertThat(JerseySimpleServerTest.invalidPropertiesKeys)
                .isNotNull();
        JerseySimpleServerTest.invalidPropertiesValues = new Properties();
        JerseySimpleServerTest.invalidPropertiesValues
                .putAll(JerseySimpleServerTest.defaultProperties);
        Assertions.assertThat(JerseySimpleServerTest.invalidPropertiesValues)
                .isNotNull();
        JerseySimpleServerTest.invalidPropertiesClazz = new Properties();
        JerseySimpleServerTest.invalidPropertiesClazz
                .putAll(JerseySimpleServerTest.defaultProperties);
        JerseySimpleServerTest.invalidPropertiesClazz.put("resource.clazz",
                "class.not.found.HelloWorldResource");
        Assertions.assertThat(JerseySimpleServerTest.invalidPropertiesClazz)
                .isNotNull();
    }

    @SuppressWarnings("static-method") // JUnit requires non-static methods
    @After
    public void tearDown() {
        JerseySimpleServerTest.server = null;
        JerseySimpleServerTest.defaultProperties = null;
        JerseySimpleServerTest.invalidPropertiesKeys = null;
        JerseySimpleServerTest.invalidPropertiesValues = null;
        JerseySimpleServerTest.invalidPropertiesClazz = null;
        Assertions.assertThat(JerseySimpleServerTest.server).isNull();
        Assertions.assertThat(JerseySimpleServerTest.defaultProperties)
                .isNull();
        Assertions.assertThat(JerseySimpleServerTest.invalidPropertiesKeys)
                .isNull();
        Assertions.assertThat(JerseySimpleServerTest.invalidPropertiesValues)
                .isNull();
        Assertions.assertThat(JerseySimpleServerTest.invalidPropertiesClazz)
                .isNull();
    }
    //CHECKSTYLE.ON: Method Javadoc

    /**
     * Test method for
     * {@link eu.connecare.HadrianJerseyServer.dss.RESTwrapper.JerseySimpleServer#validateArgs(java.lang.String[])}.
     *
     */
    @SuppressWarnings("static-method") // JUnit requires non-static methods
    @Test
    public final void testValidateArgs() {
        final SoftAssertions softly = new SoftAssertions();
        softly.assertThat(
                JerseySimpleServer.validateArgs(JerseySimpleServerTest.ARGS_0))
                .isEqualTo(JerseySimpleServerTest.DEFAULT_PROPERTY_FILE);
        softly.assertThat(
                JerseySimpleServer.validateArgs(JerseySimpleServerTest.ARGS_1))
                .isEqualTo(JerseySimpleServerTest.USER_PROPERTY_FILE);
        softly.assertThat(
                JerseySimpleServer.validateArgs(JerseySimpleServerTest.ARGS_2))
                .isEqualTo(JerseySimpleServerTest.USER_PROPERTY_FILE);
        softly.assertThat(
                JerseySimpleServer.validateArgs(JerseySimpleServerTest.ARGS_0))
                .isNotEqualTo(JerseySimpleServer
                        .validateArgs(JerseySimpleServerTest.ARGS_1));
        softly.assertThat(
                JerseySimpleServer.validateArgs(JerseySimpleServerTest.ARGS_0))
                .isNotEqualTo(JerseySimpleServer
                        .validateArgs(JerseySimpleServerTest.ARGS_2));
        softly.assertThat(
                JerseySimpleServer.validateArgs(JerseySimpleServerTest.ARGS_1))
                .isEqualTo(JerseySimpleServer
                        .validateArgs(JerseySimpleServerTest.ARGS_2));
        softly.assertAll();
    }

    /**
     * Test method for
     * {@link eu.connecare.HadrianJerseyServer.dss.RESTwrapper.JerseySimpleServer#loadProps(java.lang.String)}.
     *
     */
    @SuppressWarnings("static-method") // JUnit requires non-static methods
    @Test
    public final void testLoadProps() {
        final SoftAssertions softly = new SoftAssertions();
        try {
            softly.assertThat(JerseySimpleServer
                    .loadProps(JerseySimpleServerTest.DEFAULT_PROPERTY_FILE))
                    .isEqualTo(JerseySimpleServerTest.defaultProperties);
        } catch (final IOException e) {
            e.printStackTrace();
        }
        softly.assertThatThrownBy(() -> {
            JerseySimpleServer.loadProps("lol/i/do/not/exist.fake");
        }).isInstanceOf(IOException.class);
        softly.assertAll();
    }

    /**
     * Test method for
     * {@link eu.connecare.HadrianJerseyServer.dss.RESTwrapper.JerseySimpleServer#parseProps(java.util.Properties)}.
     *
     */
    @SuppressWarnings("static-method") // JUnit requires non-static methods
    @Test
    public final void testParseProps() {
        final SoftAssertions softly = new SoftAssertions();
        try {
            JerseySimpleServerTest.server
                    .parseProps(JerseySimpleServerTest.defaultProperties);
        } catch (final InvalidPropertiesFormatException e) {
            e.printStackTrace();
        }
        softly.assertThat(JerseySimpleServerTest.server.getEndpointIp())
                .isEqualTo(JerseySimpleServerTest.defaultProperties
                        .get("endpoint.ip"));
        softly.assertThat(JerseySimpleServerTest.server.getEndpointTcp())
                .isEqualTo(
                        Integer.valueOf(JerseySimpleServerTest.defaultProperties
                                .get("endpoint.tcp").toString()));
        softly.assertThat(JerseySimpleServerTest.server.getResourceClazz())
                .isEqualTo(JerseySimpleServerTest.defaultProperties
                        .get("resource.clazz"));
        JerseySimpleServerTest.invalidPropertiesKeys.remove("endpoint.ip");
        JerseySimpleServerTest.invalidPropertiesKeys.put("endpoint..ip",
                "127.0.0.1");
        softly.assertThatThrownBy(() -> {
            JerseySimpleServerTest.server
                    .parseProps(JerseySimpleServerTest.invalidPropertiesKeys);
        }).isInstanceOf(InvalidPropertiesFormatException.class)
                .hasMessageContaining("endpoint.ip");
        JerseySimpleServerTest.invalidPropertiesKeys.clear();
        JerseySimpleServerTest.invalidPropertiesKeys
                .putAll(JerseySimpleServerTest.defaultProperties);
        JerseySimpleServerTest.invalidPropertiesKeys.remove("endpoint.tcp");
        JerseySimpleServerTest.invalidPropertiesKeys.put("endpoint  .xcp",
                "9998");
        softly.assertThatThrownBy(() -> {
            JerseySimpleServerTest.server
                    .parseProps(JerseySimpleServerTest.invalidPropertiesKeys);
        }).isInstanceOf(InvalidPropertiesFormatException.class)
                .hasMessageContaining("endpoint.tcp");
        JerseySimpleServerTest.invalidPropertiesKeys.clear();
        JerseySimpleServerTest.invalidPropertiesKeys
                .putAll(JerseySimpleServerTest.defaultProperties);
        JerseySimpleServerTest.invalidPropertiesKeys.remove("resource.clazz");
        JerseySimpleServerTest.invalidPropertiesKeys.put("resource.class",
                "eu.connecare.cdss.rest.HelloWorldResource");
        softly.assertThatThrownBy(() -> {
            JerseySimpleServerTest.server
                    .parseProps(JerseySimpleServerTest.invalidPropertiesKeys);
        }).isInstanceOf(InvalidPropertiesFormatException.class)
                .hasMessageContaining("resource.clazz");
        JerseySimpleServerTest.invalidPropertiesValues.put("endpoint.ip",
                "http://localhost/");
        softly.assertThatThrownBy(() -> {
            JerseySimpleServerTest.server
                    .parseProps(JerseySimpleServerTest.invalidPropertiesValues);
        }).isInstanceOf(InvalidPropertiesFormatException.class)
                .hasMessageContaining("endpoint.ip");
        JerseySimpleServerTest.invalidPropertiesValues.put("endpoint.tcp",
                "99498z");
        softly.assertThatThrownBy(() -> {
            JerseySimpleServerTest.server
                    .parseProps(JerseySimpleServerTest.invalidPropertiesValues);
        }).isInstanceOf(InvalidPropertiesFormatException.class)
                .hasMessageContaining("endpoint.tcp");
        JerseySimpleServerTest.invalidPropertiesValues.put("endpoint.tcp",
                "1024");
        softly.assertThatThrownBy(() -> {
            JerseySimpleServerTest.server
                    .parseProps(JerseySimpleServerTest.invalidPropertiesValues);
        }).isInstanceOf(InvalidPropertiesFormatException.class)
                .hasMessageContaining("endpoint.tcp");
        JerseySimpleServerTest.invalidPropertiesValues.put("endpoint.tcp",
                "65537");
        softly.assertThatThrownBy(() -> {
            JerseySimpleServerTest.server
                    .parseProps(JerseySimpleServerTest.invalidPropertiesValues);
        }).isInstanceOf(InvalidPropertiesFormatException.class)
                .hasMessageContaining("endpoint.tcp");
        softly.assertThatThrownBy(() -> {
            JerseySimpleServerTest.server
                    .parseProps(JerseySimpleServerTest.invalidPropertiesClazz);
        }).isInstanceOf(InvalidPropertiesFormatException.class)
                .hasMessageContaining("resource.clazz")
                .hasCauseInstanceOf(ClassNotFoundException.class);
        softly.assertAll();
    }

    /**
     * Test method for
     * {@link eu.connecare.HadrianJerseyServer.dss.RESTwrapper.JerseySimpleServer#deploy()}.
     *
     */
    @SuppressWarnings("static-method") // JUnit requires non-static methods
    @Test
    public final void testDeploy() {
        try {
            JerseySimpleServerTest.server
                    .parseProps(JerseySimpleServerTest.defaultProperties);
        } catch (final InvalidPropertiesFormatException e) {
            e.printStackTrace();
        }
        try {
            JerseySimpleServerTest.server.deploy();
        } catch (final IOException e) {
            e.printStackTrace();
        }
        final SoftAssertions softly = new SoftAssertions();
        softly.assertThat(!JerseySimpleServerTest.server.isDown());
        JerseySimpleServerTest.server.shutdown(10, TimeUnit.SECONDS);
        softly.assertThat(!JerseySimpleServerTest.server.isDown());
        softly.assertAll();
    }

}
