/**
 * 
 */
package eu.connecare.cdss.hadrian;

import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.assertj.core.api.SoftAssertions;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

/**
 * @author sm
 *
 */
public class HadrianServiceProxyTest {

	// private static final Path ABSOLUTE_PART =
	// Paths.get("Users/sm/cdss/scala/risk-stratification/");
	private static final Path PFA_PATH = Paths.get("src/test/resources/simpleIrisModelEmit.pfa");
	private static final Path MODELS_PATH = Paths.get("models/pfa/");
	private static final Path CACHE_PATH = Paths.get("downloadcache/pfa/");
	private static final Path CSV_PATH = Paths.get("src/test/resources/iris.csv");
	private static final Path PREDICTIONS_PATH = Paths.get("predictions/");
	private static final Path RESULTS_CSV_PATH = Paths.get("src/test/resources/results.csv");
	private HadrianServiceProxy hadrian;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		this.hadrian = new HadrianServiceProxy();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		this.hadrian = null;
		FileUtils.deleteDirectory(new File(HadrianServiceProxyTest.MODELS_PATH.toString()));
		FileUtils.deleteDirectory(new File(HadrianServiceProxyTest.CACHE_PATH.toString()));
		FileUtils.deleteDirectory(new File(HadrianServiceProxyTest.PREDICTIONS_PATH.toString()));
	}

	/**
	 * Test method for
	 * {@link eu.connecare.cdss.hadrian.HadrianServiceProxy#uploadModelPfa(java.nio.file.Path)}.
	 */
	@Test
	public final void testUploadModelPfa() {
		UUID modelID = null;
		try {
			modelID = this.hadrian.uploadModelPfa(HadrianServiceProxyTest.PFA_PATH.getFileName().toString(),
					new FileInputStream(HadrianServiceProxyTest.PFA_PATH.toString()),
					"This is a " + HadrianServiceProxyTest.PFA_PATH.getFileName() + " PFA model");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		BlackBoxModel model = null;
		try {
			model = this.hadrian.modelDescriptor(modelID);
		} catch (ModelDescriptorNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		final SoftAssertions softly = new SoftAssertions();
		softly.assertThat(this.hadrian.modelFiles(modelID))
				.contains(Paths.get(HadrianServiceProxyTest.MODELS_PATH.toString(),
						modelID.toString() + "$" + HadrianServiceProxyTest.PFA_PATH.getFileName().toString()));
		/*
		 * how to define private instance members in scala besides class
		 * parameters? it seems that Scala type PFAEngine does not define
		 * equals/hashcode
		 */
		// softly.assertThat(this.hadrian.modelService(modelID))
		// .isEqualTo(new
		// HadrianService(HadrianServiceProxyTest.PFA_PATH.toString()));
		softly.assertThat(model.getName()).isEqualTo(PFA_PATH.getFileName().toString());
		checkFileDirExists(HadrianServiceProxyTest.MODELS_PATH,
				Paths.get(HadrianServiceProxyTest.MODELS_PATH.toString(),
						modelID.toString() + "$" + HadrianServiceProxyTest.PFA_PATH.getFileName().toString()),
				softly);
		try {
			HadrianServiceProxyTest.checkSameContent(HadrianServiceProxyTest.PFA_PATH,
					Paths.get(HadrianServiceProxyTest.MODELS_PATH.toString(),
							modelID.toString() + "$" + HadrianServiceProxyTest.PFA_PATH.getFileName().toString()),
					softly);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		softly.assertAll();
	}

	/**
	 * Test method for
	 * {@link eu.connecare.cdss.hadrian.HadrianServiceProxy#uploadModelRds(java.io.File)}.
	 */
	@Ignore
	@Test
	public final void testUploadModelRds() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link eu.connecare.cdss.hadrian.HadrianServiceProxy#listModels()}.
	 */
	@Test
	public final void testListModels() {
		UUID modelID = null;
		try {
			modelID = this.hadrian.uploadModelPfa(HadrianServiceProxyTest.PFA_PATH.getFileName().toString(),
					new FileInputStream(HadrianServiceProxyTest.PFA_PATH.toString()),
					"This is a " + HadrianServiceProxyTest.PFA_PATH.getFileName() + " PFA model");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		BlackBoxModel model = null;
		final UUID copyOfModelID = UUID.fromString(modelID.toString());
		try {
			model = this.hadrian.modelDescriptor(copyOfModelID);
		} catch (ModelDescriptorNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		final SoftAssertions softly = new SoftAssertions();
		softly.assertThat(this.hadrian.listModels()).contains(model);
		softly.assertAll();
	}

	/**
	 * Test method for
	 * {@link eu.connecare.cdss.hadrian.HadrianServiceProxy#modelDescriptor(java.util.UUID)}.
	 */
	@Test
	public final void testModelEngine() {
		UUID modelID = null;
		try {
			modelID = this.hadrian.uploadModelPfa(HadrianServiceProxyTest.PFA_PATH.getFileName().toString(),
					new FileInputStream(HadrianServiceProxyTest.PFA_PATH.toString()),
					"This is a " + HadrianServiceProxyTest.PFA_PATH.getFileName() + " PFA model");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		BlackBoxModel model = null;
		final UUID copyOfModelID = UUID.fromString(modelID.toString());
		try {
			model = this.hadrian.modelDescriptor(copyOfModelID);
		} catch (ModelDescriptorNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		final SoftAssertions softly = new SoftAssertions();
		softly.assertThat(model.getName()).isEqualTo(PFA_PATH.getFileName().toString());
		softly.assertThat(this.hadrian.listModels()).contains(model);
		softly.assertAll();
	}

	/**
	 * Test method for
	 * {@link eu.connecare.cdss.hadrian.HadrianServiceProxy#downloadModelPfa(java.util.UUID)}.
	 */
	@Test
	public final void testDownloadModelPfa() {
		UUID modelID = null;
		try {
			modelID = this.hadrian.uploadModelPfa(HadrianServiceProxyTest.PFA_PATH.getFileName().toString(),
					new FileInputStream(HadrianServiceProxyTest.PFA_PATH.toString()),
					"This is a " + HadrianServiceProxyTest.PFA_PATH.getFileName() + " PFA model");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Path path = null;
		try {
			path = this.hadrian.downloadModelPfa(modelID);
		} catch (PfaModelNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		final SoftAssertions softly = new SoftAssertions();
		try {
			HadrianServiceProxyTest.checkSameContent(HadrianServiceProxyTest.PFA_PATH, path, softly);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		softly.assertAll();
	}

	/**
	 * Test method for
	 * {@link eu.connecare.cdss.hadrian.HadrianServiceProxy#downloadModelRds(java.util.UUID)}.
	 */
	@Ignore
	@Test
	public final void testDownloadModelRds() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link eu.connecare.cdss.hadrian.HadrianServiceProxy#deleteModel(java.util.UUID)}.
	 */
	@Test
	public final void testDeleteModel() {
		UUID modelID = null;
		try {
			modelID = this.hadrian.uploadModelPfa(HadrianServiceProxyTest.PFA_PATH.getFileName().toString(),
					new FileInputStream(HadrianServiceProxyTest.PFA_PATH.toString()),
					"This is a " + HadrianServiceProxyTest.PFA_PATH.getFileName() + " PFA model");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		final SoftAssertions softly = new SoftAssertions();
		try {
			softly.assertThat(this.hadrian.listModels()).contains(this.hadrian.modelDescriptor(modelID));
		} catch (ModelDescriptorNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		softly.assertThat(this.hadrian.modelService(modelID)).isNotNull();
		softly.assertThat(this.hadrian.modelFiles(modelID)).isNotNull();
		boolean success = false;
		try {
			success = this.hadrian.deleteModel(modelID);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		softly.assertThat(success);
		try {
			softly.assertThat(this.hadrian.listModels()).doesNotContain(this.hadrian.modelDescriptor(modelID));
		} catch (ModelDescriptorNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// softly.assertThat(this.hadrian.modelService(modelID)).isNull();
		final UUID finalModelID = modelID; // see
											// https://stackoverflow.com/questions/25894509/problems-with-local-variable-scope-how-to-solve-it
		softly.assertThatThrownBy(() -> {
			this.hadrian.modelService(finalModelID);
		}).hasMessage("No service exists for the given UUID");
		softly.assertThat(this.hadrian.modelFiles(modelID)).isNull();
		softly.assertAll();
	}

	/**
	 * Test method for
	 * {@link eu.connecare.cdss.hadrian.HadrianServiceProxy#predictSingle(java.util.UUID, eu.connecare.cdss.hadrian.DataPoint)}.
	 */
	@Ignore
	@Test
	public final void testPredictSingle() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link eu.connecare.cdss.hadrian.HadrianServiceProxy#predictBatch(java.util.UUID, java.util.List)}.
	 */
	@Ignore
	@Test
	public final void testPredictBatch() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link eu.connecare.cdss.hadrian.HadrianServiceProxy#predictBatchJson(java.util.UUID, java.io.File)}.
	 */
	@Ignore
	@Test
	public final void testPredictBatchJson() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link eu.connecare.cdss.hadrian.HadrianServiceProxy#predictBatchCsv(java.util.UUID, java.io.File)}.
	 */
	@Test
	public final void testPredictBatchCsv() {
		UUID modelID = null;
		try {
			modelID = this.hadrian.uploadModelPfa(HadrianServiceProxyTest.PFA_PATH.getFileName().toString(),
					new FileInputStream(HadrianServiceProxyTest.PFA_PATH.toString()),
					"This is a " + HadrianServiceProxyTest.PFA_PATH.getFileName() + " PFA model");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Path predictionsCsv = null;
		try {
			predictionsCsv = this.hadrian.predictBatchCsv(modelID,
					HadrianServiceProxyTest.CSV_PATH.getFileName().toString(),
					Files.newInputStream(HadrianServiceProxyTest.CSV_PATH), false);
		} catch (PredictionServiceNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		final SoftAssertions softly = new SoftAssertions();
		checkFileDirExists(HadrianServiceProxyTest.PREDICTIONS_PATH, predictionsCsv, softly);
		try {
			HadrianServiceProxyTest.checkSameContent(HadrianServiceProxyTest.RESULTS_CSV_PATH, predictionsCsv, softly);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		softly.assertAll();
	}

	private void checkFileDirExists(final Path dir, final Path file, final SoftAssertions softly) {
		softly.assertThat(Files.exists(dir)).isTrue();
		softly.assertThat(Files.notExists(dir)).isFalse();
		softly.assertThat(Files.exists(file)).isTrue();
		softly.assertThat(Files.notExists(file)).isFalse();
	}

	private static void checkSameContent(final Path pathExpected, final Path pathActual, final SoftAssertions softly)
			throws IOException {
		// final InputStream is1 = Files.newInputStream(pathExpected);
		// final InputStream is2 = Files.newInputStream(pathActual);
		// byte[] bytes1 = new byte[1024];
		// byte[] bytes2 = new byte[1024];
		// while (is1.read(bytes1) != -1 && is2.read(bytes2) != -1) {
		// softly.assertThat(bytes1).isEqualTo(bytes2);
		// }
		softly.assertThat(FileUtils.contentEquals(pathExpected.toFile(), pathActual.toFile())).isTrue();
	}

}
